package com.superteam.superim.friend.service;

import com.superim.account.vo.UserVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/6
 * Time: 9:36
 * Code introduction:
 */
@FeignClient(value = "AccountServer")
public interface ConsumerService {

    @PostMapping("/selectUserOne.do")
    public UserVo selectUserOne(String account);


    @PostMapping("/selectUsers.do")
    public List<UserVo> selectUsers(List accounts);
}
