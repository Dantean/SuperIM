package com.superteam.superim.friend.service.impl;

import com.superim.account.vo.UserVo;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.buddy.ApplicationAddFriendList;
import com.superteam.superim.entity.buddy.FriendsGroup;
import com.superteam.superim.entity.buddy.FriendsList;
import com.superteam.superim.entity.user.User;
import com.superteam.superim.friend.dao.FriendDao;
import com.superteam.superim.friend.service.ConsumerService;
import com.superteam.superim.friend.service.FriendService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/5
 * Time: 15:45
 * Code introduction:
 */

@Service
public class FriendServiceImpl implements FriendService {
    @Resource
    private FriendDao friendDao;
    @Resource
    private ConsumerService selectUserByAccount;

    /**
     * 查分组内所有的好友
     * @param myaccount
     * @param groupNmae
     * @return
     */
    @Override
    public R selectFriendByGroup(String myaccount, String groupNmae) {
        Map map = new HashMap();
        map.put("MyAccount",myaccount);
        map.put("grouping",groupNmae);
        List<FriendsList> friendsLists = friendDao.selectFriendByGroup(map);
        return R.ok(friendsLists);
    }

    /**
     * 查询所有的好友
     * @param token
     * @return
     */
    @Override
    public List<FriendsList> selectFriend(String token) {
        //User str = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        List<FriendsList> strings = friendDao.selectFriend(token);

        return strings;
    }

    /**
     * 查询单个好友
     * @param token
     * @param friendsAccount
     * @return
     */
    @Override
    public R selectOneFriend(String token,String friendsAccount) {
        Map map = new HashMap();
        map.put("MyAccount",token);
        map.put("FriendsAccount",friendsAccount);
        FriendsList friendsList = friendDao.selectOneFriend(map);
        return R.ok(friendsList);
    }


    /**
     * 修改备注
     * @param token
     * @param FriendsAccount
     * @param Remarks
     * @return
     */
    @Override
    public R changeNotes(String token, String FriendsAccount, String Remarks) {

        Map<String,String> map = new HashMap();
        map.put("MyAccount",token);
        map.put("FriendsAccount",FriendsAccount);
        map.put("Remarks",Remarks);
        int i = friendDao.changeNotes(map);
        if (i==1){
            return R.ok("修改成功");
        }else {
            return R.fail("修改失败");
        }
    }


    /**
     * 删除好友
     * @param token
     * @param FriendsAccount
     * @return
     */
    @Override
    public R deleteFriend(String token, String FriendsAccount) {
        User str = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        Map<String,String> map = new HashMap();
        map.put("MyAccount",str.getAccount());
        map.put("FriendsAccount",FriendsAccount);
        int i = friendDao.deleteFriend(map);
        if (i==1){
            return R.ok("删除成功");
        }else {
            return R.fail("删除失败");
        }
    }

    /**
     * 删除分组
     * @param myaccount
     * @param groupNmae
     * @return
     */
    @Override
    public R deleteGroup(String myaccount, String groupNmae) {
        if (groupNmae.equals("我的好友")){
            return R.fail("默认分组，无法删除");
        }else {
            Map<String,String> map = new HashMap();
            map.put("MyAccount",myaccount);
            map.put("grouping",groupNmae);
            int i = friendDao.deleteGroup(map);
            if (i==1){
                //分组删除成功后将该分组中的所有好友移动到默认分组
                friendDao.updateFriendsGroup(map);
                return R.ok("删除成功");
            }else {
                return R.fail("删除失败");
            }
        }
    }



    /**
     * 添加分组
     * @param myaccount
     * @param groupNmae
     * @return
     */
    @Override
    public R addGroup(String myaccount, String groupNmae) {
        FriendsGroup friendsGroup = new FriendsGroup();
        friendsGroup.setAccount(myaccount);
        friendsGroup.setGrouping(groupNmae);
        int i = friendDao.addGroup(friendsGroup);
        if (i==1){
            return R.ok("添加成功");
        }else {
            return R.fail("添加失败");
        }
    }

    /**
     * 审核添加好友
     * @param applicant
     * @param reviewer
     * @param status
     * @return
     */
    @Override
    public R reviewAdd(String applicant, String reviewer, String status) {
        ApplicationAddFriendList addFriendList = new ApplicationAddFriendList();
        addFriendList.setReviewer(reviewer);
        addFriendList.setApplicant(applicant);
        addFriendList.setStatus(status);
        //通过申请人账号获取申请人信息
        UserVo userVo = selectUserByAccount.selectUserOne(applicant);

        int i = friendDao.updateStatus(addFriendList);
        if (status.equals("通过")){
            FriendsList friendsList = new FriendsList();
            friendsList.setMyAccount(reviewer);
            friendsList.setFriendsAccount(applicant);
            friendsList.setRemarks(userVo.getNickName());//将申请人的昵称设为备注名
            friendDao.addFriend(friendsList);
            return R.ok();
        }else {
            return R.ok();
        }
    }


    /**
     * 申请添加好友
     * @param myaccount
     * @param FriendsAccount
     * @return
     */
    @Override
    public R applicationAddFriend(String myaccount, String FriendsAccount,String remarks) {
        Map map = new HashMap();
        map.put("MyAccount",myaccount);
        map.put("FriendsAccount",FriendsAccount);
        FriendsList friendsList = friendDao.selectOneFriend(map);

        ApplicationAddFriendList applicationAddFriendList = new ApplicationAddFriendList();
        applicationAddFriendList.setRemarks(remarks);
        applicationAddFriendList.setApplicant(FriendsAccount);
        applicationAddFriendList.setReviewer(myaccount);
        //判断是否已经是好友
        if (friendsList==null){
            int i = friendDao.applicationAdd(applicationAddFriendList);
            return R.ok("申请成功，请静候佳音");
        }else {
            UserVo userVo = selectUserByAccount.selectUserOne(FriendsAccount);
            return R.ok(userVo);
        }
    }

    /**
     * 查询所有分组
     * @param token
     * @return
     */
    @Override
    public R selectFriendsGroup(String token) {
        List<FriendsGroup> friendsGroups = friendDao.selectFriendsGroup(token);
        return R.ok(friendsGroups);
    }

    /**
     * 查询申请添加的好友列表
     * @param myaccount
     * @return
     */
    @Override
    public R QueryFriendApplicationList(String myaccount) {
        List<ApplicationAddFriendList> applicationAddFriendLists = friendDao.selectApplicationAddFriendList(myaccount);
        return R.ok(applicationAddFriendLists);
    }
}
