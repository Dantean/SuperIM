package com.superteam.superim.friend.dao;

import com.superteam.superim.entity.buddy.ApplicationAddFriendList;
import com.superteam.superim.entity.buddy.FriendsGroup;
import com.superteam.superim.entity.buddy.FriendsList;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/5
 * Time: 14:50
 * Code introduction:
 */

public interface FriendDao {
    //查分组内所有的好友
    List<FriendsList> selectFriendByGroup(Map map);

    //查询所有的好友账号
    List<FriendsList> selectFriend(String MyAccount);

    //查询单个好友
    FriendsList selectOneFriend(Map map);

    //修改备注
    int changeNotes(Map map);

    //删除好友
    int deleteFriend(Map map);

    //删除分组
    int deleteGroup(Map map);

    //删除分组后将该分组的好友放到默认分组中=我的好友
    int updateFriendsGroup(Map map);

    //增加分组
    int addGroup(FriendsGroup friendsGroup);

    //查询申请添加好友的信息
    List<ApplicationAddFriendList> selectApplicationAddFriendList(String reviewer);

    //添加好友
    //更新审核状态
    int updateStatus(ApplicationAddFriendList applicationAddFriendList);
    //添加到好友列表中
    int addFriend(FriendsList friendsList);

    //申请添加好友
    int applicationAdd(ApplicationAddFriendList applicationAddFriendList);

    //查询所有的分组
    List<FriendsGroup> selectFriendsGroup(String account);

}
