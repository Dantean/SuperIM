package com.superteam.superim.friend.controller;

import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.config.SystemCofig;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.buddy.FriendsList;
import com.superteam.superim.entity.user.User;
import com.superteam.superim.friend.service.FriendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/5
 * Time: 15:53
 * Code introduction:
 */
@Api(tags = "好友管理")
@RestController
@RequestMapping("/friend")
public class FriendController {
    @Autowired
    private FriendService friendService;


    @ApiOperation(value = "查分组内所有的好友", notes = "查分组内所有的好友")
    @GetMapping("/selectFriendByGroup.do")
    public R selectFriendByGroup(HttpServletRequest request,String groupNmae){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return friendService.selectFriendByGroup(user.getAccount(),groupNmae);
    }

    /**
     * 查询所有的好友
     * @param request
     * @return
     */
    @ApiOperation(value = "查询所有的好友", notes = "查询所有的好友")
    @GetMapping("/selectAllFriend.do")
    public R selectAllFriend(HttpServletRequest request){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        List<FriendsList> integers = friendService.selectFriend(user.getAccount());
        return R.ok(integers);
    }

    /**
     * 查询单个好友
     * @param request
     * @param friendsAccount
     * @return
     */
    @ApiOperation(value = "查询单个好友", notes = "查询单个好友")
    @GetMapping("/selectOneFriend.do")
    public R selectOneFriend(HttpServletRequest request,String friendsAccount){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return friendService.selectOneFriend(user.getAccount(),friendsAccount);
    }

    /**
     * 更改备注
     * @param request 获取当前登录用户的token
     * @param FriendsAccount 需要更改朋友备注的账号
     * @param Remarks 更改的备注
     * @return 更改是否成功
     */
    @ApiOperation(value = "更改好友备注", notes = "更改好友备注")
    @PostMapping("/changeNotes.do")
    public R changeNotes(HttpServletRequest request,String FriendsAccount,String Remarks){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return  friendService.changeNotes(user.getAccount(), FriendsAccount, Remarks);
    }

    /**
     * 删除好友，单方面删除
     * @param request 请求信息
     * @param FriendsAccount 要删除朋友的账户
     * @return
     */
    @ApiOperation(value = "删除好友", notes = "删除好友")
    @PostMapping("/deleteFriend.do")
    public R deleteFriend(HttpServletRequest request,String FriendsAccount){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return  friendService.deleteFriend(user.getAccount(), FriendsAccount);
    }

    /**
     * 删除分组
     * @param request 请求信息
     * @param groupNmae 要删除的分组名
     * @return
     */
    @ApiOperation(value = "删除分组", notes = "删除分组")
    @PostMapping("/deleteGroup.do")
    public R deleteGroup(HttpServletRequest request,String groupNmae){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return  friendService.deleteGroup(user.getAccount(), groupNmae);
    }


    /**
     * 添加分组
     * @param request 请求信息
     * @param groupNmae 分组的名字
     * @return
     */
    @ApiOperation(value = "添加分组", notes = "添加分组")
    @PostMapping("/addGroup.do")
    public R addGroup(HttpServletRequest request,String groupNmae){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return  friendService.addGroup(user.getAccount(), groupNmae);
    }

    /**
     * 审核添加好友
     * @param applicant 申请人qq
     * @param request 请求信息
     * @param status 更改审核状态
     * @return
     */
    @ApiOperation(value = "审核添加好友", notes = "审核添加好友")
    @PostMapping("/reviewAdd.do")
    public R reviewAdd(String applicant,HttpServletRequest request, String status){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return  friendService.reviewAdd(applicant, user.getAccount(),status);
    }

    /**
     * 申请添加好友
     * @param request 请求信息
     * @param FriendsAccount 要添加的账号
     * @param remarks 备注信息
     * @return
     */
    @ApiOperation(value = "申请添加好友", notes = "申请添加好友")
    @PostMapping("/applicationAddFriend.do")
    public R applicationAddFriend(HttpServletRequest request, String FriendsAccount,String remarks){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return  friendService.applicationAddFriend(user.getAccount(), FriendsAccount,remarks);
    }

    /**
     * 查询所有分组
     * @param request 请求信息
     * @return
     */
    @ApiOperation(value = "查询所有分组", notes = "查询所有分组")
    @PostMapping("/selectFriendsGroup.do")
    public R selectFriendsGroup(HttpServletRequest request){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return  friendService.selectFriendsGroup(user.getAccount());
    }

    /**
     * 查询好友申请列表
     * @param request 请求信息
     * @return
     */
    @ApiOperation(value = "查询好友申请列表", notes = "查询好友申请列表")
    @PostMapping("/QueryFriendApplicationList.do")
    public R QueryFriendApplicationList(HttpServletRequest request){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return friendService.QueryFriendApplicationList(user.getAccount());
    }

}
