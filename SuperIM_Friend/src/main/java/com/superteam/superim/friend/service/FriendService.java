package com.superteam.superim.friend.service;

import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.buddy.FriendsGroup;
import com.superteam.superim.entity.buddy.FriendsList;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/5
 * Time: 15:45
 * Code introduction:
 */

public interface FriendService {
    //查分组内所有的好友
    R selectFriendByGroup(String myaccount,String groupNmae);

    //查询所有的好友账号
    List<FriendsList> selectFriend(String token);

    //查询单个好友信息
    R selectOneFriend(String token,String FriendsAccount);

    //修改好友备注
    R changeNotes(String token, String FriendsAccount, String Remarks);

    //删除好友
    R deleteFriend(String token, String FriendsAccount);

    //删除分组
    R deleteGroup(String myaccount,String groupNmae);

    //添加分组
    R addGroup(String myaccount,String groupNmae);

    //审核添加好友
    R reviewAdd(String applicant,String reviewer,String status);

    //申请添加好友
    R applicationAddFriend(String myaccount,String FriendsAccount,String remarks);

    //查询所有的分组
    R selectFriendsGroup(String token);

    //查询申请添加的好友列表
    R QueryFriendApplicationList(String myaccount);
}
