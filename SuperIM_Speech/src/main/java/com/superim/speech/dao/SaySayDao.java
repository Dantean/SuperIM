package com.superim.speech.dao;

import com.superteam.superim.common.dto.SaySayDto;
import com.superteam.superim.entity.SaySay;
import org.springframework.stereotype.Repository;

@Repository
public interface SaySayDao {
    //    发朋友圈
    int addSaySay(SaySay saySay);

    //    存图片
    int addSaySayImg(String imgPath, int sid);

    //    转发
    int addSaySayForward(SaySay saySay);

    //    通过说说id查找说说对象(不包含图片)
    SaySay selectSaySayById(Integer id);

    //    通过sid查找对象(包含图片)
    SaySayDto selectSaySayBySid(Integer sid);
}