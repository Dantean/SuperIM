package com.superim.speech.dao;

import com.superteam.superim.entity.Comment;
import com.superteam.superim.entity.Reply;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentDao {
    //    评论
    int addComment(Comment comment);

    //    回复评论
    int addReply(Reply reply);
}
