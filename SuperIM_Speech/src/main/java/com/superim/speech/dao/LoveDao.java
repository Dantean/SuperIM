package com.superim.speech.dao;

import com.superteam.superim.entity.Love;
import org.springframework.stereotype.Repository;

@Repository
public interface LoveDao {
//    添加收藏
    int addLove(Love love);
}
