package com.superim.speech.controller;

import com.superim.speech.service.intf.AccountService;
import com.superteam.superim.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("调用Account获取用户信息")
@RestController
@RequestMapping("/")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "通过uid查询用户信息", notes = "通过uid查询用户信息")
    @GetMapping("/selectUserOne.do")
    public R selectUserOne(Integer uid) {
        String account = uid.toString();
        return R.ok(accountService.selectUserOne(account));
    }
}
