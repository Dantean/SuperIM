package com.superim.speech.controller;

import com.superim.speech.service.intf.PraiseService;
import com.superteam.superim.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "关于说说赞/取消点赞")
@RestController
@RequestMapping("/praise")
public class PraiseController {

    @Autowired
    private PraiseService praiseService;

    @ApiOperation(value = "说说点赞/取消点赞", notes = "说说点赞/取消点赞")
    @PostMapping("/aboutPraise.do")
    public R aboutPraise(int id, String account) {
        long praiseCount = praiseService.pointPraise(id, account);
        return R.ok(praiseCount);
    }
}