package com.superim.speech.controller;

import com.superim.speech.service.intf.CommentService;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.Comment;
import com.superteam.superim.entity.Reply;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comment")
@Api(tags = "评论相关操作")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/addComment.do")
    @ApiOperation(value = "评论", notes = "评论")
    public R addComment(Comment comment) {
        commentService.addComment(comment);
        return R.ok();
    }

    @PostMapping("/addReply.do")
    @ApiOperation(value = "回复评论", notes = "回复评论")
    public R addComment(Reply reply) {
        commentService.addReply(reply);
        return R.ok();
    }
}
