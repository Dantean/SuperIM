package com.superim.speech.controller;

import com.superim.speech.service.intf.LoveService;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.Love;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "说说的收藏")
@RequestMapping("/love")
@RestController
public class LoveController {
    @Autowired
    private LoveService loveService;

    @PostMapping("/addLove.do")
    @ApiOperation(value = "添加收藏", notes = "添加收藏")
    public R addLove(Love love) {
        loveService.addLove(love);
        return R.ok();
    }
}
