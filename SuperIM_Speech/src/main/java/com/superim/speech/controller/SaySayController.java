package com.superim.speech.controller;

import com.superim.speech.service.intf.SaySayService;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.SaySay;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/saySay")
@Api(tags = "空间相关操作")
public class SaySayController {

    @Autowired
    private SaySayService saySayService;

    @ApiOperation(value = "发说说(不包含sid)/转发说说(包含sid)", notes = "发说说(不包含sid)/转发说说(包含sid)")
    @PostMapping("/addSaySay.do")
    public R addSaySay(SaySay saySay, List<MultipartFile> fileList) {
        saySayService.addSaySay(saySay, fileList);
        return R.ok();
    }

    @ApiOperation(value = "通过说说id查找说说对象(不包含图片)", notes = "通过说说id查找说说对象(不包含图片)")
    @GetMapping("/selectSaySayById.do")
    public R selectSaySayById(Integer id) {
        return R.ok(saySayService.selectSaySayById(id));
    }

    @ApiOperation(value = "通过sid查找对象(包含图片)", notes = "通过sid查找对象(包含图片)")
    @GetMapping("/selectSaySayBySId.do")
    public R selectSaySayBySid(Integer sid) {
        return R.ok(saySayService.selectSaySayBySid(sid));
    }


}
