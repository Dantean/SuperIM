package com.superim.speech;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement //开启事务
@MapperScan(basePackages = "com.superim.speech.dao")   //设置dao的扫描
@EnableFeignClients         //互相调用服务
@EnableDiscoveryClient      //服务的注册和发现
public class SpeechServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpeechServerApplication.class, args);
    }
}