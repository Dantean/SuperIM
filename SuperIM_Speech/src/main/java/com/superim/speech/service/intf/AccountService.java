package com.superim.speech.service.intf;

import com.superim.account.vo.UserVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "AccountServer")
public interface AccountService {

    @GetMapping("/selectUserOne.do")
    UserVo selectUserOne(String account);
}
