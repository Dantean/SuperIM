package com.superim.speech.service.intf;

import com.superteam.superim.common.dto.SaySayDto;
import com.superteam.superim.entity.SaySay;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

//@FeignClient(value = "")
public interface SaySayService {
    //    发朋友圈
    int addSaySay(SaySay saySay, List<MultipartFile> fileList);

    //    通过说说id查找说说对象(不包含图片)
    SaySay selectSaySayById(Integer id);

    //    通过sid查找对象(包含图片)
    SaySayDto selectSaySayBySid(Integer sid);

}
