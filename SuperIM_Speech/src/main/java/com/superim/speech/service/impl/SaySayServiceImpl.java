package com.superim.speech.service.impl;

import com.superim.speech.dao.SaySayDao;
import com.superim.speech.service.intf.SaySayService;
import com.superteam.superim.common.dto.SaySayDto;
import com.superteam.superim.common.util.ContentCensorUtil;
import com.superteam.superim.common.util.OssUtil;
import com.superteam.superim.common.util.StringUtil;
import com.superteam.superim.entity.SaySay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@Transactional
public class SaySayServiceImpl implements SaySayService{

    @Autowired
    private SaySayDao saySayDao;

    //    发朋友圈
    @Override
    public int addSaySay(SaySay saySay, List<MultipartFile> fileList) {

//        根据saySay.sid是否为空来判断  发说说还是转发说说
        if (saySay.getSid() != null) {
//            文本审核
            if (ContentCensorUtil.censorText(saySay.getText())) {
//                保存数据库
                saySayDao.addSaySayForward(saySay);
            } else {
                return -1;
            }

        } else {

            //        审核并保存文本内容
            if (ContentCensorUtil.censorText(saySay.getText())) {
                saySayDao.addSaySay(saySay);
            } else {
                return -1;
            }
        }

        for (MultipartFile file : fileList) {

            if (!file.isEmpty()) {
//            获取上传的文件名

                String fn = file.getOriginalFilename();
//            获取上传的文件内容
                try {
                    byte[] data = file.getBytes();

//                内容审核 鉴别图片是否合法
                    if (ContentCensorUtil.censorImg(data)) {
//                对文件名进行重命名
                        fn = OssUtil.rename(fn);

//                将文件上传到阿里云的OSS

                        String url = "";
                        try {
                            url = OssUtil.upload(fn, data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (StringUtil.isnoEmpty(url)) {
//                            保存图片
                            saySayDao.addSaySayImg(url, saySay.getId());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return 1;
    }

    //    通过说说id查找说说对象(不包含图片)
    @Override
    public SaySay selectSaySayById(Integer id) {
        return saySayDao.selectSaySayById(id);
    }

    //    通过sid查找对象(包含图片)
    @Override
    public SaySayDto selectSaySayBySid(Integer sid) {
        return saySayDao.selectSaySayBySid(sid);
    }
}