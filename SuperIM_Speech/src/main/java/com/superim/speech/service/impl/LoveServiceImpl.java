package com.superim.speech.service.impl;

import com.superim.speech.dao.LoveDao;
import com.superim.speech.service.intf.LoveService;
import com.superteam.superim.entity.Love;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoveServiceImpl implements LoveService {

    @Autowired
    private LoveDao loveDao;

//    添加收藏
    @Override
    public int addLove(Love love) {
        return loveDao.addLove(love);
    }
}
