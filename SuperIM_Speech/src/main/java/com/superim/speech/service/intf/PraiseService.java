package com.superim.speech.service.intf;

public interface PraiseService {
    long pointPraise(int id, String account);
}
