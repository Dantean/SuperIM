package com.superim.speech.service.intf;


import com.superteam.superim.entity.Comment;
import com.superteam.superim.entity.Reply;

public interface CommentService {
    //    评论
    int addComment(Comment comment);

    //    回复评论
    int addReply(Reply reply);
}
