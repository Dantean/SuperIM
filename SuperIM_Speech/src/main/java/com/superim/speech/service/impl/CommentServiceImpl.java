package com.superim.speech.service.impl;

import com.superim.speech.dao.CommentDao;
import com.superim.speech.service.intf.CommentService;
import com.superteam.superim.entity.Comment;
import com.superteam.superim.entity.Reply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;

//    评论
    @Override
    public int addComment(Comment comment) {
        return commentDao.addComment(comment);
    }

//    回复评论
    @Override
    public int addReply(Reply reply) {
        return commentDao.addReply(reply);
    }
}
