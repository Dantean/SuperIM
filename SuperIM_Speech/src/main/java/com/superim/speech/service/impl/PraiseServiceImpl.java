package com.superim.speech.service.impl;

import com.superim.speech.service.intf.PraiseService;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.JedisUtil;
import org.springframework.stereotype.Service;
 @Service
public class PraiseServiceImpl implements PraiseService {
    @Override
    public long pointPraise(int id, String account) {
//        获取JedisUtil对象
        JedisUtil instance = JedisUtil.getInstance();
//        把用户账户存入redis的set集合中  如果是点赞返回1  取消点赞返回0(取消点赞的话,删除set中此用户)
        long setnx = instance.SETS.sAdd(RedisKeyConfig.PRAISE_USERS + id, account);

        if (setnx == 0) {
//            删除集合中取消点赞的用户
            instance.SETS.srem(RedisKeyConfig.PRAISE_USERS + id, account);
        }
        return instance.SETS.scard(RedisKeyConfig.PRAISE_USERS + id);
    }
}