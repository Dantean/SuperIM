package com.superteam.superim.chat.service.impl;

import com.superteam.superim.chat.service.MsgGroupService;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.chat.MsgGroup;
import com.superteam.superim.chat.dao.MsgGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 
 * @author: hiseage
 * @create: 2020-10-06 14:04:45
 */
@Service
public class MsgGroupServiceImpl implements MsgGroupService {

    @Resource
    private MsgGroupDao msgGroupDao;

    @Override
    public int insert(MsgGroup msgGroup) {
        return msgGroupDao.insert(msgGroup);
    }

    @Override
    public int deleteById(int id) {
        return msgGroupDao.deleteById(id);
    }

    @Override
    public int update(MsgGroup msgGroup) {
        return msgGroupDao.update(msgGroup);
    }

    @Override
    public List<MsgGroup> select(MsgGroup msgGroup) {
        return msgGroupDao.select(msgGroup);
    }
}

