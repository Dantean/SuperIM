package com.superteam.superim.chat.service;

import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.chat.MsgGroup;
import java.util.List;

/**
 * @description: 
 * @author: hiseage
 * @create: 2020-10-06 14:04:45
 */
public interface MsgGroupService {
    // 增
    int insert(MsgGroup msgGroup);
    // 删
    int deleteById(int id);
    // 改
    int update(MsgGroup msgGroup);
    // 查
    List<MsgGroup> select(MsgGroup msgGroup);
}