package com.superteam.superim.chat.dao;

import com.superteam.superim.entity.chat.MsgSingle;
import java.util.List;

/**
 * @description: 
 * @author: hiseage
 * @create: 2020-10-06 14:04:45
 */
public interface MsgSingleDao {
    // 增
    int insert(MsgSingle msgSingle);
    // 删
    int deleteById(int id);
    // 改
    int update(MsgSingle msgSingle);
    // 查
    List<MsgSingle> select(MsgSingle msgSingle);


}



