package com.superteam.superim.chat.socket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.superteam.superim.chat.config.MyEndpointConfigure;
import com.superteam.superim.chat.service.MsgGroupService;
import com.superteam.superim.chat.service.MsgSingleService;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.ContentCensorUtil;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.entity.chat.MsgGroup;
import com.superteam.superim.entity.chat.MsgSingle;
import com.superteam.superim.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author guo
 * @date 2020-10-06 14:05
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//@RestController
//@RequestMapping("/chat/")
@ServerEndpoint(value = "/chat/{id}", configurator = MyEndpointConfigure.class)
public class WebSocketServer implements Serializable {
    @Resource
    private MsgSingleService singleService;
    @Resource
    private MsgGroupService groupService;

    private Integer fromId;

    /**
     * 在线用户的Map集合，key：用户名，value：Session对象
     */
    private static Map<Object, Session> sessionMap = new ConcurrentHashMap<>();

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("id") Integer id) {
        fromId = id;

        sessionMap.put(id, session);

        //通知除了自己之外的所有人
        //sendOnlineCount(session, "{'type':'onlineCount','onlineCount':" + WebSocketServer.onlineCount + ",username:'" + username + "'}");
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        //RedissonUtil.delField(RedisKeyConfig.SOCKET_SESSION, type+ ":" + id);

        for (Entry<Object, Session> entry : sessionMap.entrySet()) {
            if (entry.getValue() == session) {
                sessionMap.remove(entry.getKey());
                break;
            }
        }

        //通知除了自己之外的所有人
        //sendOnlineCount(session, "{'type':'onlineCount','onlineCount':" + WebSocketServer.onlineCount + ",username:'" + logoutUserName + "'}");
    }

    /**
     * 服务器接收到客户端消息时调用的方法
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println(message);
        //if (ContentCensorUtil.censorText(message)) {

            // 解析message
            try {
                HashMap hashMap = new ObjectMapper().readValue(message, HashMap.class);

                //消息类型 1.私聊 2.群聊
                Integer type = (Integer) hashMap.get("type");
                //来源用户
                //Integer fromId = (Integer) hashMap.get("fromId");
                hashMap.put("fromId", fromId);
                //目标用户
                Integer toId = (Integer) hashMap.get("toId");

                String content = (String) hashMap.get("message");

                if (type == 1) {
                    singleService.insert(new MsgSingle(fromId, toId, content, new Date()));

                    privateChat(session, toId, hashMap);
                } else if (type == 2) {
                    groupService.insert(new MsgGroup(fromId, toId, content, new Date()));

                    groupChat(session, toId, hashMap);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }


        //}
    }

    /**
     * 发生错误时调用
     */
    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }

    /**
     * 私聊
     */
    private void privateChat(Session session, Integer toId, HashMap hashMap) throws IOException {
        Session toSession = sessionMap.get(toId);

        //如果不在线则发送“对方不在线”回来源用户

            if (toSession == null) {
                session.getBasicRemote().sendText("{\"type\":\"0\",\"message\":\"对方不在线\"}");
            } else {
                toSession.getBasicRemote().sendText(new ObjectMapper().writeValueAsString(hashMap));
            }

    }

    /**
     * 群聊
     */
    private void groupChat(Session session, Integer toId, HashMap hashMap) throws IOException {
        List<User> users = new ArrayList<>();

        // TODO 与群的相关接口对接
        for (int i = 1; i < 5; i++) {
            User user1 = new User();
            user1.setId(i);
            users.add(user1);
        }


        for (User user : users) {
            Session toSession = sessionMap.get(user.getId());
            if (toSession == null) {

            } else if (!session.equals(toSession)) {

                toSession.getBasicRemote().sendText(new ObjectMapper().writeValueAsString(hashMap));

            }
        }
    }


}
