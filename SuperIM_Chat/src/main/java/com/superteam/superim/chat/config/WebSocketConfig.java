package com.superteam.superim.chat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;


/**
 * @program: LiveRoom
 * @description:
 * @author: Feri(邢朋辉)
 * @create: 2020-09-27 11:53
 */
@Configuration
public class WebSocketConfig {
    //WebSocket服务端  发布解析器
    @Bean
    public ServerEndpointExporter createSEE(){
        return new ServerEndpointExporter();
    }

    /**
     * 支持注入其他类
     */
    @Bean
    public MyEndpointConfigure  newMyEndpointConfigure (){
        return new MyEndpointConfigure ();
    }
}