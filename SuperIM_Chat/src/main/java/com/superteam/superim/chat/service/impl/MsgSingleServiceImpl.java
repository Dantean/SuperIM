package com.superteam.superim.chat.service.impl;

import com.superteam.superim.chat.service.MsgSingleService;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.chat.MsgSingle;
import com.superteam.superim.chat.dao.MsgSingleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 
 * @author: hiseage
 * @create: 2020-10-06 14:04:45
 */
@Service
public class MsgSingleServiceImpl implements MsgSingleService {

    @Resource
    private MsgSingleDao msgSingleDao;

    @Override
    public int insert(MsgSingle msgSingle) {
        return msgSingleDao.insert(msgSingle);
    }

    @Override
    public int deleteById(int id) {
        return msgSingleDao.deleteById(id);
    }

    @Override
    public int update(MsgSingle msgSingle) {
        return msgSingleDao.update(msgSingle);
    }

    @Override
    public List<MsgSingle> select(MsgSingle msgSingle) {
        return msgSingleDao.select(msgSingle);
    }
}

