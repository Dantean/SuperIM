package com.superteam.superim.sign;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-05 14:32
 **/
@SpringBootApplication  // 开关类
@EnableDiscoveryClient  // 服务的注册和发现
@MapperScan("com.superteam.superim.sign.dao")   // 扫描dao层
@EnableFeignClients // 启用服务调用
public class SignApplication {
    public static void main(String[] args) {
        SpringApplication.run(SignApplication.class,args);
    }
}
