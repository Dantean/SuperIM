package com.superteam.superim.sign.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-06 18:01
 **/
public class RealIP {
    public static String getIP() {
        /*获取用户登录ip*/
        ProcessBuilder process = new ProcessBuilder("curl", "icanhazip.com");
        Process p;
        try {
            p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer buffer = new StringBuffer();
            String line = null;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
//                buffer.append(System.getProperty("line.separator"));  换行

                return buffer.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
