package com.superteam.superim.sign.controller;

import com.superteam.superim.common.config.SystemCofig;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.sign.service.intf.UserSignService;
import com.superteam.superim.sign.utils.HttpIP;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-05 14:58
 **/
@Api(tags = "用户额外功能")
@RestController
@RequestMapping("/user/")
public class UserSignController {

    @Autowired
    private UserSignService userSignService;

    // 实现签到
    @ApiOperation(value = "用户签到", notes = "新增用户签到信息")
    @PostMapping("sign/sign.do")
    public R sign(HttpServletRequest request) {
        return userSignService.sign(request.getHeader(SystemCofig.HEAD_TOKEN));
    }

    // 上传图片到相册
    @ApiOperation(value = "批量上传图片", notes = "批量上传图片到相册")
    @PostMapping("upload/upload.do")
    public R upload(HttpServletRequest request, List<MultipartFile> files) {
        return userSignService.upload(request.getHeader(SystemCofig.HEAD_TOKEN), files);
    }

    // 查看我的相册
    @ApiOperation(value = "查看我的相册", notes = "查看我的相册")
    @GetMapping("photo/watch.do")
    public R watch(HttpServletRequest request) {
        return userSignService.watch(request.getHeader(SystemCofig.HEAD_TOKEN));
    }

    // 查看天气
    @ApiOperation(value = "查看天气", notes = "查看所在地天气")
    @GetMapping("weather/weather.do")
    public R getWeather() {
        return userSignService.getWeather();
    }

    //查看我的等级
    @ApiOperation(value = "查看我的等级", notes = "查看我的等级")
    @GetMapping("grade/selectGrade")
    public R selectGrade(HttpServletRequest request){
        return userSignService.selectGrade(request.getHeader(SystemCofig.HEAD_TOKEN));
    }

}
