package com.superteam.superim.sign.entity;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-06 18:31
 **/
@lombok.Data
public class Data {
    private String ip;
    private String long_ip;
    private String isp;
    private String area;
    private String region_id;
    private String region;
    private String city_id;
    private String city;
    private String country_id;
    private String country;

}
