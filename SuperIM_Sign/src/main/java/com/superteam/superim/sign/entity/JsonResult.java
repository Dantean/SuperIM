package com.superteam.superim.sign.entity;

import lombok.Data;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-06 18:29
 **/
@Data
public class JsonResult {
    private Integer ret;
    private String msg;
    private String log_id;
    private com.superteam.superim.sign.entity.Data data;
}
