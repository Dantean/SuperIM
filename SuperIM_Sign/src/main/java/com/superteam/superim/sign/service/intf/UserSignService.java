package com.superteam.superim.sign.service.intf;

import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.user.Grade;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserSignService {
    // 签到
    R sign(String token);

    // 上传图片到相册
    R upload(String token, List<MultipartFile> files);

    // 查看我的相册
    R watch(String token);

    // 查看所在地天气
    R getWeather();

    // 查看我的等级
    R selectGrade(String token);
}
