package com.superteam.superim.sign.test;

import com.superteam.superim.sign.third.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: superim
 * @description: 经纬度解析为地理位置
 * @author: ErBa(韩智飞)
 * @create: 2020-10-07 12:36
 **/
public class CoordinatesToAddress {
    public static void main(String[] args) {
        String host = "https://jisujwddz.market.alicloudapi.com";
        String path = "/geoconvert/coord2addr";
        String method = "ANY";
        String appcode = "c1a6abf239ba4cb4a91e864a046a5640";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/json; charset=UTF-8");
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("lat", "40.235686");
        querys.put("lng", "122.115235");
        querys.put("type", "google");


        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            System.out.println(response.toString());
//            获取response的body
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
