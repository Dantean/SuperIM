package com.superteam.superim.sign.test;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.superteam.superim.sign.utils.UrlToFile;

import java.io.File;
import java.util.HashMap;

/**
 * @program: superim
 * @description: 获取图片经纬度
 * @author: ErBa(韩智飞)
 * @create: 2020-10-06 10:45
 **/
public class ExifTest {
    public static HashMap<String,String> getCoordinates(String url) throws Exception {
        File file = UrlToFile.getFile(url);
        Metadata metadata = JpegMetadataReader.readMetadata(file);
        HashMap<String, String> map = new HashMap<>();
        for(Directory directory : metadata.getDirectories()){
            for(Tag tag : directory.getTags()){
                if (tag.getTagName().equals("GPS Latitude")) {  // 维度
                    map.put("latitude",tag.getDescription());
                }
                if (tag.getTagName().equals(" GPS Longitude")) {  // 经度
                    map.put("longitude",tag.getDescription());
                }

            }
        }
        return map;
    }
}
