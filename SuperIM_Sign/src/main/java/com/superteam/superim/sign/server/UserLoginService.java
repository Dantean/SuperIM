package com.superteam.superim.sign.server;

import com.superteam.superim.common.vo.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "AccountServer")
public interface UserLoginService {

    @PostMapping("/account/login.do")
    R login(@RequestParam("account") String account, @RequestParam("password") String password);
}
