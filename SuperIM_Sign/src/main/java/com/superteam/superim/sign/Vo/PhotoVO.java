package com.superteam.superim.sign.Vo;

import lombok.Data;

/**
 * @program: superim
 * @description: 照片
 * @author: ErBa(韩智飞)
 * @create: 2020-10-07 14:28
 **/
@Data
public class PhotoVO {
    private String photo;
    private String country;
    private String province;
    private String city;
    private String district;
}
