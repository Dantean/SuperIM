package com.superteam.superim.sign.dao;

import com.superteam.superim.entity.user.Grade;
import com.superteam.superim.entity.user.UserPhotos;
import com.superteam.superim.entity.user.UserSign;
import com.superteam.superim.sign.Vo.MyPhotoVO;

import java.util.List;

public interface UserSignDao {

    // 查询最后一次签到数据
    UserSign selectByUid(int uid);

    // 新增签到记录
    int insert(UserSign userSign);

    // 新增照片信息
    int addPhoto(UserPhotos userPhotos);

    // 查询登陆用户的相册
    List<MyPhotoVO> selectMyPhotos(Integer uid);

    // 查询用户等级
    Grade selectGrade(Integer uid);
}
