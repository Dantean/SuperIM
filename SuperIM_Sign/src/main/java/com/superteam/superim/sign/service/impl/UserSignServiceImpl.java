package com.superteam.superim.sign.service.impl;

import com.alibaba.fastjson.JSON;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.DateUtil;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.entity.user.Grade;
import com.superteam.superim.entity.user.User;
import com.superteam.superim.entity.user.UserPhotos;
import com.superteam.superim.entity.user.UserSign;
import com.superteam.superim.sign.Vo.MyPhotoVO;
import com.superteam.superim.sign.dao.UserSignDao;
import com.superteam.superim.sign.entity.Data;
import com.superteam.superim.sign.entity.JsonResult;
import com.superteam.superim.sign.service.intf.UserSignService;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.sign.test.IPToAddress;
import com.superteam.superim.sign.test.RealIP;
import com.superteam.superim.sign.test.Weather;
import com.superteam.superim.sign.third.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-05 15:15
 **/
@Service
public class UserSignServiceImpl implements UserSignService {
    @Autowired
    private UserSignDao userSignDao;

    @Override
    public R sign(String token) {
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        // 1. 通过令牌获取用户id
        Integer uid = user.getId();
        UserSign userSign = new UserSign();
        // 2. 查询最后一次签到数据
        UserSign sign = userSignDao.selectByUid(uid);
        int days = 1;
        // 3. 验证今天是否签过到
        if (DateUtil.checkDate(sign.getStime(),new Date())) {
            // 签到日期 等于今天
            return R.fail("亲, 您今天已签过到，请明天再来");
        } else {
            // 4. 验证是否连续签到
            if (DateUtil.checkYesdate(sign.getStime())){
                // 连续签到
                days = sign.getDays() + 1;
            }
            // 断签  days = 1;

        }
        // 5.新增签到信息
        userSign.setDays(days);
        userSign.setUid(uid);
        userSign.setStime(new Date());
        int insert = userSignDao.insert(userSign);
        if (insert > 0) {
            return R.ok("亲，您已连续签到"+ days + "天");
        } else {
            return R.fail("签到失败");
        }
    }

    @Override
    public R upload(String token, List<MultipartFile> files) {
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        // 1. 通过令牌获取用户id
        Integer uid = user.getId();
        // 判断是否有文件
        if (files == null) {
            return R.ok("亲，照片不能为空哦");
        }
        if (files.size() > 0) {
            for (MultipartFile file : files) {
                // 获取每个上传图片的文件名
                String fn = file.getOriginalFilename();
                // 将文件内容转为 字节数组
                byte[] data = fn.getBytes();
                // 对文件名进行长度和名字的重新调整
                fn = OssUtil.rename(fn);
                //将文件上传到阿里云的css
                String url = OssUtil.upload(fn, data);
                String ip = RealIP.getIP();
                System.out.println("我要的ip：" + ip);
                String adress = IPToAddress.getAdress("223.88.91.175");
                System.out.println(adress);
                JsonResult jsonResult = JSON.parseObject(adress, JsonResult.class);
                if (jsonResult.getRet() != 200) {
                    return R.fail("ip解析错误");
                }
                Data resultData = jsonResult.getData();
                String province = resultData.getRegion();   // 省份
                String city = resultData.getCity();         // 城市
                String country = resultData.getCountry();   // 国家
                System.out.println("详细地址：" + country + province + city);
                UserPhotos userPhotos = new UserPhotos();
                userPhotos.setUid(uid);
                userPhotos.setPhoto(url);
                userPhotos.setCountry(country);
                userPhotos.setProvince(province);
                userPhotos.setUtime(new Date());
                userPhotos.setCity(city);
                int i = userSignDao.addPhoto(userPhotos);
                if (i < 0) {
                    return R.fail("上传失败");
                }
                return R.ok(url);
            }
        }
        return R.fail("新增失败");

    }

    @Override
    public R watch(String token) {
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        List<MyPhotoVO> myPhotoVOS = userSignDao.selectMyPhotos(user.getId());
        return R.ok(myPhotoVOS);
    }

    @Override
    public R getWeather() {
        String ip = RealIP.getIP();
        System.out.println(ip);
        String weather = Weather.getWeather("223.88.91.250");
        return R.ok(weather);
    }


    @Override
    public R selectGrade(String token) {
        User user = (User) RedissonUtil.getStr(RedisKeyConfig.LOGIN_ACCOUNT + token);
        Grade grade = userSignDao.selectGrade(user.getId());
        return R.ok(grade);
    }
}
