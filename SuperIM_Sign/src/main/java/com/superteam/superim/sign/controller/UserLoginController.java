package com.superteam.superim.sign.controller;

import com.superteam.superim.common.vo.R;
import com.superteam.superim.sign.server.UserLoginService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-06 10:23
 **/
@Api(tags = "用户登录测试")
@RestController
@RequestMapping("/user/")
public class UserLoginController {
    @Autowired
    private UserLoginService userLoginService;

    @PostMapping("login.do")
    public R login(@RequestParam("account") String account, @RequestParam("password") String password) {
        return userLoginService.login(account, password);
    }
}
