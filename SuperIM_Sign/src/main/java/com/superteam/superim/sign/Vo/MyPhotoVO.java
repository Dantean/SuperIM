package com.superteam.superim.sign.Vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @program: superim
 * @description: 我的相册 按照时间进行归类
 * @author: ErBa(韩智飞)
 * @create: 2020-10-07 13:30
 **/
@Data
public class MyPhotoVO {
    private Integer id;
    @JsonFormat(pattern = "yyyy年MM月dd日",timezone = "GMT+8")
    private String utime;
    private List<PhotoVO> photoVOList;

}
