package com.superim.account.service.impl;

import com.superim.account.dao.AccountDao;
import com.superim.account.service.intf.RegisterService;
import com.superim.account.util.EncryptUtil;
import com.superim.account.util.OssUtil;
import com.superim.account.util.StringUtil;
import com.superim.account.vo.RegisterUserVo;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.ContentCensorUtil;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.common.vo.R;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author HZF
 */
@Service
public class RegisterServiceImpl implements RegisterService {
    @Resource
    private AccountDao accountDao;

    @Value("${SuperIMAccount.rsa.privateKey}")
    private String privateKey;

    @Override
    public R registerUser(String nickName, String password, String phone, MultipartFile file, String code) {
        if (RedissonUtil.checkKey(RedisKeyConfig.SMS_CODE + phone) && file != null) {
            String s = RedissonUtil.getStr(RedisKeyConfig.SMS_CODE + phone).toString();
            if (code.equals(s)) {
                String a = accountDao.selectLastAccount();
                int ac = Integer.parseInt(a);
                String account = (ac + 1) + "";

                boolean flag = false;
                /*靓号保存下来，可以单独对外出售*/
                while (true) {
                    if (Pattern.matches("^\\d*(\\d)\\1(\\d)\\2\\d*$", account)) {
                        if (account.equals(accountDao.selectLastPrettyAccount())) {
                            break;
                        }
                        accountDao.savePrettyNumber(account);
                        int temp = Integer.parseInt(account);
                        account = (temp + 1) + "";
                    } else if (Pattern.matches("^\\d*(\\d)(\\d)\\1\\2$", account)) {
                        if (account.equals(accountDao.selectLastPrettyAccount())) {
                            break;
                        }
                        accountDao.savePrettyNumber(account);
                        int temp = Integer.parseInt(account);
                        account = (temp + 1) + "";
                    } else if (Pattern.matches("^\\d*(\\d)\\1(\\d)\\2$", account)) {
                        if (account.equals(accountDao.selectLastPrettyAccount())) {
                            break;
                        }
                        accountDao.savePrettyNumber(account);
                        int temp = Integer.parseInt(account);
                        account = (temp + 1) + "";
                    } else {
                        break;
                    }
                }

                String p = EncryptUtil.rsaEnc(privateKey, password);

                RegisterUserVo registerUserVo = new RegisterUserVo();
                registerUserVo.setNickName(nickName);
                registerUserVo.setAccount(account);
                registerUserVo.setPassword(p);
                registerUserVo.setPhone(phone);

                try {
                    String filename = file.getOriginalFilename();
                    byte[] data = file.getBytes();
                    if (ContentCensorUtil.censorImg(data)) {
                        filename = OssUtil.rename(filename);
                        String url = OssUtil.upload(filename, data);
                        if (StringUtil.isNoEmpty(url)) {
                            registerUserVo.setPhoto(url);
                        }
                    } else {
                        return R.fail("头像违规");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                accountDao.insertRegisterUser(registerUserVo);

                return R.ok(account);
            } else {
                return R.fail("验证码错误");
            }
        }
        return R.fail("验证码失效和上传头像");
    }
}
