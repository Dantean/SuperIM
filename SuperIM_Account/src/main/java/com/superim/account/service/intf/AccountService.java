package com.superim.account.service.intf;

import com.superteam.superim.common.vo.R;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author HZF
 */
public interface AccountService {
    /**
     * 用户登录
     * @param account 账号
     * @param password 密码
     * @return 返回用户登录信息，令牌
     */
    public R login(String account, String password);

    /**
     * 通过手机验证码登录
     * @param account 用户账号
     * @param phone 手机号
     * @param code 验证码
     * @return 返回用户登录的信息， 令牌
     */
    public R loginByPhone(String account, String phone, String code);

    /**
     * 根据Token删除当前登录的用户登录状态
     * @param token 当前登录用户信息
     * @return 是否成功
     */
    public R loginOutUser(String token);

    /**
     * 用户找回密码
     * @param account 找回密码的账户
     * @param phone 注册时填写的手机号
     * @param code 手机实时验证码
     * @param newPassword 新的密码
     * @param name 实名认证时填写的姓名
     * @param idCardNum 实名认证时填写的身份证号码
     * @return 是否成功
     */
    public R retrievePas(String account, String phone, String code, String newPassword, String name, String idCardNum);

    /**
     * 修改注册信息
     * @param token 需要用户登录
     * @param newFile 新的头像
     * @param newPhone 新的手机号
     * @param code 新手机号的验证码
     * @param newNickName 新的昵称
     * @return 是否修改成功
     */
    public R modifyInfo(String token, MultipartFile newFile, String newPhone, String code, String newNickName);
}
