package com.superim.account.service.intf;

import com.superteam.superim.common.vo.R;

/**
 * @author HZF
 */
public interface SmsService {
    /**
     * 发送手机号获取验证码
     * @param phone 手机号
     * @return 验证码
     */
    public R sendCodeSms(String phone);
}
