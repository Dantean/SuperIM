package com.superim.account.service.intf;

import com.superim.account.vo.UserVo;

import java.util.List;

/**
 * @author HZF
 */
public interface SelectUserByAccount {
    /**
     * 通过一个account查找信息
     * @param account 指定账户
     * @return 查找到的信息
     */
    public UserVo selectUserOne(String account);

    /**
     * 通过一个list集合查找多个账户信息
     * @param accounts list集合
     * @return 查找到的信息
     */
    public List<UserVo> selectUsers(List accounts);
}
