package com.superim.account.service.intf;

import com.superteam.superim.common.vo.R;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author HZF
 */
public interface LegalizeUserService {
    /**
     * 用户实名认证
     * @param idName 用户手动输入的姓名和身份证上的图片进行比对看是否相同
     * @param idCardNum 用户手动输入的身份证号和身份证上的图片进行比对看是否相同
     * @param token 实名认证需要登录判断是哪个用户在实名
     * @param phone 手机号码
     * @param code 手机验证码
     * @param file 用户上传的身份证正面照片
     * @return 实名认证结果
     */
    public R legalizeUser(String idName, String idCardNum, String phone, String code, MultipartFile file, String token);
}
