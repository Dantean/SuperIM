package com.superim.account.service.impl;

import com.superim.account.dao.AccountDao;
import com.superim.account.service.intf.SelectUserByAccount;
import com.superim.account.vo.UserVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author HZF
 */
@Service
public class SelectUserByAccountImpl implements SelectUserByAccount {
    @Resource
    private AccountDao accountDao;

    @Override
    public UserVo selectUserOne(String account) {
        System.out.println("数据测试" + account);
        System.out.println(accountDao.selectUserOne(account));
        return accountDao.selectUserOne(account);
    }

    @Override
    public List<UserVo> selectUsers(List accounts) {
        return accountDao.selectUsers(accounts);
    }
}
