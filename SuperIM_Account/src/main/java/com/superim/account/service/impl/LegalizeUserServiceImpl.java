package com.superim.account.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.superim.account.dao.AccountDao;
import com.superim.account.service.intf.LegalizeUserService;
import com.superim.account.util.OrcIdCard;
import com.superim.account.util.OssUtil;
import com.superim.account.util.StringUtil;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.ContentCensorUtil;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.user.User;
import com.superteam.superim.entity.user.Verified;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author HZF
 */
@Service
public class LegalizeUserServiceImpl implements LegalizeUserService {
    @Resource
    private AccountDao accountDao;

    @Override
    public R legalizeUser(String idName, String idCardNum, String phone, String code, MultipartFile file, String token) {
        String s = RedissonUtil.getStr(RedisKeyConfig.SMS_CODE + phone).toString();
        User user = (User) RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);

        if (code.equals(s)) {

            if (user != null) {
                Integer uid = user.getId();
                Verified ver = accountDao.selectLoginUserLegalize(uid);

                if (ver.getId() < 0) {
                    if (file != null) {
                        try {
                            String filename = file.getOriginalFilename();
                            byte[] data = file.getBytes();
                            if (ContentCensorUtil.censorImg(data)) {
                                filename = OssUtil.rename(filename);
                                String url = OssUtil.upload(filename, data);
                                if (StringUtil.isNoEmpty(url)) {
                                    String msg = OrcIdCard.getIdCardMsg(url);
                                    JSONObject jsonObject = JSON.parseObject(msg);

                                    String address = jsonObject.getString("address");
                                    String idCardNumber = jsonObject.getString("num");
                                    String sex = jsonObject.getString("sex");
                                    String birth = jsonObject.getString("birth");
                                    String nationality = jsonObject.getString("nationality");
                                    String name = jsonObject.getString("name");

                                    if (idName.equals(name) && idCardNum.equals(idCardNumber)) {
                                        Verified verified = new Verified();
                                        verified.setAddress(address);
                                        verified.setIdCardNum(idCardNumber);
                                        verified.setSex(sex);
                                        verified.setBirth(birth);
                                        verified.setNationality(nationality);
                                        verified.setIdName(name);
                                        verified.setUid(uid);
                                        verified.setIdCardPositive(url);
                                        accountDao.insertLegalizeUserInfo(verified);
                                        return R.ok("实名认证成功!");

                                    } else {
                                        return R.fail("您输入的身份证号和姓名有误！");
                                    }

                                } else {
                                    return R.fail("服务器接收图片失败");
                                }

                            } else {
                                return R.fail("请上传身份证正面");
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        return R.fail("身份证照片不能为空！");
                    }
                } else {
                    return R.fail("该用户已实名认证");
                }

            } else {
                return R.fail("请先登录");
            }
        }
        return R.fail("手机验证码不正确");
    }
}
