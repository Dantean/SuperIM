package com.superim.account.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.superim.account.dao.AccountDao;
import com.superim.account.service.intf.AccountService;
import com.superim.account.util.*;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.ContentCensorUtil;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.user.LoginOut;
import com.superteam.superim.entity.user.User;
import com.superteam.superim.entity.user.Verified;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * @author HZF
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Resource
    private AccountDao accountDao;

    @Value("${SuperIMAccount.rsa.privateKey}")
    private String privateKey;

    /**记录当前登录用户的id*/
    private int id = -1;

    /**
     * 用于获取当前登录的地址
     * country 国家
     * region 省份
     * city 城市
     */
    private enum adder {
        /**
         * country 国家
         * region 省份
         * city 城市
         */
        country, region, city
    }

    @Override
    public R login(String account, String password) {
        User loginUser = accountDao.selectByAccount(account);
        if (loginUser != null) {
            // 把用户输入的密码转换为密文和数据库中的密文比较
            if (loginUser.getPassword().equals(EncryptUtil.rsaEnc(privateKey, password))) {
                // 当前账号在线 唯一登录 挤掉之前登录的
                if (RedissonUtil.checkKey(RedisKeyConfig.LOGIN_ACCOUNT + account)) {
                    String oldToken = (String) RedissonUtil.getStr(RedisKeyConfig.LOGIN_ACCOUNT + account);
                    // 新增挤掉信息
                    RedissonUtil.addStr(RedisKeyConfig.LOGIN_JD + RedissonUtil.getStr(RedisKeyConfig.LOGIN_ACCOUNT + account), System.currentTimeMillis());
                    // 删除原来的数据
                    RedissonUtil.delKey(RedisKeyConfig.LOGIN_ACCOUNT + account, RedisKeyConfig.LOGIN_TOKEN + oldToken);
                }
                // 登录成功 - 生成令牌
                String token = JwtCore.createToken(account);
                // 存储到 Redis 唯一登录 一个账号只能在线一个 String类型redis
                // IM:login:account: 账户 记录当前账号的登录状态
                RedissonUtil.addStrTime(RedisKeyConfig.LOGIN_ACCOUNT + account, token, 1800);
                // 记录令牌对应的用户信息
                // redis  要解决 存入对象 序列化问题
                RedissonUtil.addStrTime(RedisKeyConfig.LOGIN_TOKEN + token, loginUser, 1800);

                /*获取用户登录ip*/
                ProcessBuilder process = new ProcessBuilder("curl", "icanhazip.com");
                Process p;
                try {
                    p = process.start();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    StringBuffer buffer = new StringBuffer();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                        buffer.append(System.getProperty("line.separator"));
                    }

                    String adders = GetIpUtil.getAdders(buffer.toString());
                    JSONObject jsonObject = JSON.parseObject(adders);
                    JSONObject data = jsonObject.getJSONObject("data");
                    if (data == null) {
                        return R.fail("ip解析错误");
                    }

                    StringBuffer b = new StringBuffer();
                    for (adder a : adder.values()) {
                        b.append(data.getString(String.valueOf(a)));
                    }

                    String loginAddress = accountDao.selectLastLoginAddress(loginUser.getId());
                    if (loginAddress.equals(b.toString())) {
                        //记录登录时间
                        LoginOut login = new LoginOut();
                        login.setUid(loginUser.getId());
                        login.setLoginTime(new Date());
                        login.setLoginIp(buffer.toString());
                        login.setLoginAddress(b.toString());
                        accountDao.insertLoginTime(login);
                        id = login.getId();
                    } else {
                        return R.fail("异地登录!,请用手机验证码登录");
                    }
                } catch (IOException e) {
                    System.out.print("服务器错误");
                }
                // 返回令牌
                return R.ok(token);
            }
        }
        return R.fail("登录失败");
    }

    @Override
    public R loginByPhone(String account, String phone, String code) {
        User loginUser = accountDao.selectByAccount(account);
        if (loginUser.getPhone() != null) {
            if (loginUser.getPhone().equals(phone)) {
                if (RedissonUtil.checkKey(RedisKeyConfig.SMS_CODE + phone)) {
                    String s = RedissonUtil.getStr(RedisKeyConfig.SMS_CODE + phone).toString();
                    if (code.equals(s)) {
                        // 当前账号在线 唯一登录 挤掉之前登录的
                        if (RedissonUtil.checkKey(RedisKeyConfig.LOGIN_ACCOUNT + account)) {
                            String oldToken = (String) RedissonUtil.getStr(RedisKeyConfig.LOGIN_ACCOUNT + account);
                            // 新增挤掉信息
                            RedissonUtil.addStr(RedisKeyConfig.LOGIN_JD + RedissonUtil.getStr(RedisKeyConfig.LOGIN_ACCOUNT + account), System.currentTimeMillis());
                            // 删除原来的数据
                            RedissonUtil.delKey(RedisKeyConfig.LOGIN_ACCOUNT + account, RedisKeyConfig.LOGIN_TOKEN + oldToken);
                        }
                        // 登录成功 - 生成令牌
                        String token = JwtCore.createToken(account);
                        // 存储到 Redis 唯一登录 一个账号只能在线一个 String类型redis
                        // IM:login:account: 账户 记录当前账号的登录状态
                        RedissonUtil.addStrTime(RedisKeyConfig.LOGIN_ACCOUNT + account, token, 1800);
                        // 记录令牌对应的用户信息
                        // redis  要解决 存入对象 序列化问题
                        RedissonUtil.addStrTime(RedisKeyConfig.LOGIN_TOKEN + token, loginUser, 1800);

                        /*获取用户登录ip*/
                        ProcessBuilder process = new ProcessBuilder("curl", "icanhazip.com");
                        Process p;
                        try {
                            p = process.start();
                            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            StringBuffer buffer = new StringBuffer();
                            String line = null;
                            while ((line = reader.readLine()) != null) {
                                buffer.append(line);
                                buffer.append(System.getProperty("line.separator"));
                            }

                            String adders = GetIpUtil.getAdders(buffer.toString());
                            JSONObject jsonObject = JSON.parseObject(adders);
                            JSONObject data = jsonObject.getJSONObject("data");
                            if (data == null) {
                                return R.fail("ip解析错误");
                            }

                            StringBuffer b = new StringBuffer();
                            for (adder a : adder.values()) {
                                b.append(data.getString(String.valueOf(a)));
                            }

                            //记录登录时间
                            LoginOut login = new LoginOut();
                            login.setUid(loginUser.getId());
                            login.setLoginTime(new Date());
                            login.setLoginIp(buffer.toString());
                            login.setLoginAddress(b.toString());
                            accountDao.insertLoginTime(login);
                            id = login.getId();
                        } catch (IOException e) {
                            System.out.print("服务器错误");
                        }
                        // 返回令牌
                        return R.ok(token);
                    } else {
                        return R.fail("验证码错误");
                    }
                } else {
                    return R.fail("验证码失效");
                }
            } else {
                return R.fail("手机号错误");
            }
        }
        return R.fail("该账户没有绑定手机号");
    }

    @Override
    public R loginOutUser(String token) {
        User user = (User) RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);

        /*获取用户注销时ip*/
        ProcessBuilder process = new ProcessBuilder("curl", "icanhazip.com");
        Process p;
        try {
            p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer buffer = new StringBuffer();
            String line = null;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
                buffer.append(System.getProperty("line.separator"));
            }

            String adders = GetIpUtil.getAdders(buffer.toString());
            JSONObject jsonObject = JSON.parseObject(adders);
            JSONObject data = jsonObject.getJSONObject("data");
            if (data == null) {
                return R.fail("ip解析错误");
            }

            StringBuffer b = new StringBuffer();
            for (adder a : adder.values()) {
                b.append(data.getString(String.valueOf(a)));
            }

            //记录退出时间
            LoginOut quit = new LoginOut();
            quit.setQuitTime(new Date());
            quit.setQuitIp(buffer.toString());
            quit.setQuitAddress(b.toString());
            quit.setId(id);
            accountDao.updateQuitTime(quit);
            RedissonUtil.delKey(RedisKeyConfig.LOGIN_TOKEN + token);
            RedissonUtil.delKey(RedisKeyConfig.LOGIN_ACCOUNT + user.getAccount());
            return R.ok("退出成功");
        } catch (IOException e) {
            System.out.print("服务器错误");
        }
        return R.fail("退出失败");
    }

    @Override
    public R retrievePas(String account, String phone, String code, String newPassword, String name, String idCardNum) {
        User user = accountDao.selectByAccount(account);

        if (user != null) {

            if (user.getPhone().equals(phone)) {
                String s = RedissonUtil.getStr(RedisKeyConfig.SMS_CODE + phone).toString();

                if (code.equals(s)) {
                    Verified verified = accountDao.selectLoginUserLegalize(user.getId());

                    if (verified != null) {
                        if (verified.getIdName().equals(name) && verified.getIdCardNum().equals(idCardNum)) {
                            String rsaEnc = EncryptUtil.rsaEnc(privateKey, newPassword);
                            int updatePassword = accountDao.updatePassword(account, rsaEnc);
                            if (updatePassword > 0) {
                                return R.ok("成功");
                            } else {
                                return R.fail("更新失败请重试");
                            }
                        } else {
                            return R.fail("实名认证信息不正确");
                        }
                    } else {
                        return R.fail("请先实名认证");
                    }

                } else {
                    return R.fail("验证码错误");
                }
            } else {
                return R.fail("请输入正确的手机号");
            }
        }
        return R.fail("该账户不存在");
    }

    @Override
    public R modifyInfo(String token, MultipartFile newFile, String newPhone, String code, String newNickName) {
        User user = (User) RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        User u = new User();

        if (user != null) {
            u.setId(user.getId());

            if (newFile != null || newPhone != null || newNickName != null) {

                if (newNickName != null && !"".equals(newNickName)) {
                    u.setNickName(newNickName);
                }

                if (newFile != null) {
                    try {
                        String filename = newFile.getOriginalFilename();
                        byte[] data = newFile.getBytes();
                        if (ContentCensorUtil.censorImg(data)) {
                            filename = OssUtil.rename(filename);
                            String url = OssUtil.upload(filename, data);
                            if (StringUtil.isNoEmpty(url)) {
                                u.setPhoto(url);
                            }
                        } else {
                            return R.fail("头像违规");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (newPhone != null && !"".equals(newPhone)) {
                    String s = RedissonUtil.getStr(RedisKeyConfig.SMS_CODE + newPhone).toString();

                    if (code.equals(s)) {
                        u.setPhone(newPhone);
                    } else {
                        return R.fail("手机验证码错误");
                    }
                }

                int i = accountDao.updateUserInfo(u);
                if (i > 0) {
                    return R.ok("成功");
                } else {
                    return R.fail("修改失败.");
                }

            } else {
                return R.fail("请输入您想要修改的资料");
            }
        }

        return R.fail("请先登录");
    }
}
