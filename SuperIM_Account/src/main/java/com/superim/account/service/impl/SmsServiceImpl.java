package com.superim.account.service.impl;

import com.superim.account.service.intf.SmsService;
import com.superim.account.util.AliSmsUtil;
import com.superim.account.util.NumRandomUtil;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.common.vo.R;
import org.springframework.stereotype.Service;

/**
 * @author HZF
 */
@Service
public class SmsServiceImpl implements SmsService {
    @Override
    public R sendCodeSms(String phone) {
        int code;
        if (RedissonUtil.checkKey(RedisKeyConfig.SMS_CODE + phone)) {
            // 原来的验证码没有失效
            code = Integer.parseInt((RedissonUtil.getStr(RedisKeyConfig.SMS_CODE + phone)).toString());
        } else {
            // 如果验证码失效或者不存在，再次随机生成6位数的验证码
            code = NumRandomUtil.createNum(6);
        }
        if(AliSmsUtil.sendCodeSms(phone, code)){
            // TODO 手机验证码测试时设置的时间和验证码内容不对应需要修改
            RedissonUtil.addStrTime(RedisKeyConfig.SMS_CODE + phone, code+"",6000);
            return R.ok(code);
        }
        return R.fail();
    }
}
