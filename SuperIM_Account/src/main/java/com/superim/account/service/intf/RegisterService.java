package com.superim.account.service.intf;

import com.superteam.superim.common.vo.R;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author HZF
 */
public interface RegisterService {
    /**
     * 注册服务
     * @param nickName 昵称
     * @param password 密码
     * @param phone 手机号
     * @param file 头像
     * @param code 注册手机验证码
     * @return 返回系统指定账号不可更改且唯一
     */
    public R registerUser(String nickName, String password, String phone, MultipartFile file, String code);
}
