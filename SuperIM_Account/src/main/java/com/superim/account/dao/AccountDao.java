package com.superim.account.dao;

import com.superim.account.vo.RegisterUserVo;
import com.superim.account.vo.UserVo;
import com.superteam.superim.entity.user.LoginOut;
import com.superteam.superim.entity.user.User;
import com.superteam.superim.entity.user.Verified;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HZF
 */
@Component
public interface AccountDao {
    /**
     * 通过账户查找数据库中是否有该用户
     * @param account 用户登录账户
     * @return 查找到的用户信息
     */
    public User selectByAccount(@Param("account")String account);

    /**
     * 通过一个account查找信息
     * @param account 指定账户
     * @return 查找到的信息
     */
    public UserVo selectUserOne(@Param("account")String account);

    /**
     * 通过一个list集合查找多个账户信息
     * @param accounts list集合
     * @return 查找到的信息
     */
    public List<UserVo> selectUsers(List accounts);

    /**
     * 用户登录时插入登录时间
     * @param loginOut 通过用户id进行插入
     */
    public void insertLoginTime(LoginOut loginOut);

    /**
     * 查询最后一个出现的靓号
     * @return 返回账号
     */
    public String selectLastPrettyAccount();

    /**
     * 查询最后一个User表中的账号
     * @return 查找到的账户
     */
    public String selectLastAccount();

    /**
     * 获取最后一次登录的登录地址，判断是否异地登录
     * @param id 通过用户id
     * @return 返回登录地址
     */
    public String selectLastLoginAddress(int id);

    /**
     * 插入用户注册填写的信息
     * @param registerUserVo 用户注册填写的信息
     */
    public void insertRegisterUser(RegisterUserVo registerUserVo);

    /**
     * 用户退出
     * @param loginOut 用户退出表实体类
     */
    public void updateQuitTime(LoginOut loginOut);

    /**
     * 靓号保存到新的数据表
     * @param prettyNum 靓号数据
     */
    public void savePrettyNumber(String prettyNum);

    /**
     * 用户实名认证信息
     * @param verified 实名认证信息实体类
     */
    public void insertLegalizeUserInfo(Verified verified);

    /**
     * 查询用户是否实名认证过，认证过之后不需要再次认证
     * @param uid 用户的id
     * @return 是否能查找到用户实名认证的信息
     */
    public Verified selectLoginUserLegalize(int uid);

    /**
     * 用户新密码
     * @param account 对应账户
     * @param password 密文新密码
     * @return 是否更新成功
     */
    public int updatePassword(@Param("account")String account, @Param("password")String password);

    /**
     * 修改用户注册信息
     * @param user 包含用户修改的信息
     * @return 是否修改成功
     */
    public int updateUserInfo(User user);
}
