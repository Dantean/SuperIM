package com.superim.account.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HZF
 */
@Data
@NoArgsConstructor
public class RegisterUserVo {
    private String nickName;
    private String account;
    private String password;
    private String phone;
    private String photo;
}
