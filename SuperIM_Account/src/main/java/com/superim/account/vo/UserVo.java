package com.superim.account.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author HZF
 */
@Data
@NoArgsConstructor
@ToString
public class UserVo implements Serializable {
    private Integer id;
    private String nickName;
    private String account;
    private String phone;
    private String photo;
}
