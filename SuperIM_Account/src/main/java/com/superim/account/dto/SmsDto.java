package com.superim.account.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author HZF
 */
@Data
@NoArgsConstructor
public class SmsDto {
    private String code;
    private String message;
}
