package com.superim.account.util;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.superim.account.dto.SmsDto;

/**
 * @author HZF
 */
public class AliSmsUtil {

    private static final String accessKeyId = "LTAI4G3aWEKBSDER6M76hGEM";
    private static final String accessKeySecret = "zgNJtwNL2LZzDIS4Way5Rf45pdicTC";
    private static final String signname="在线问诊";
    private static final String tempcode="SMS_202815800";
    /**
     *  发送验证码
     * @param phone 手机号
     * @param code  验证码
     * @return boolean类型 true 发送成功  false 发送失败
     */
    public static boolean sendCodeSms(String phone, int code) {
        DefaultProfile profile = DefaultProfile.getProfile(
                "cn-hangzhou", accessKeyId, accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", signname);
        request.putQueryParameter("TemplateCode", tempcode);
        request.putQueryParameter("TemplateParam", "{\"code\":"+ code +"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            // 获取短信验证码返回的json字符串数据
            String json = response.getData();
            if (StringUtil.isNoEmpty(json)) {
                // 将json字符串转换为json对象
                SmsDto smsDto = JSON.parseObject(json, SmsDto.class);
                // 通过code 获取 信息是否发送成功
                if (smsDto.getCode().equals("OK")) {
                    return true;
                }
            }
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }
}
