package com.superim.account.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.Callback;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.UploadFileRequest;

import java.io.ByteArrayInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author HZF
 */
public class OssUtil {

    private static final String endpoint = "oss-cn-beijing.aliyuncs.com";
    private static final String accessKeyId = "LTAI4G3aWEKBSDER6M76hGEM";
    private static final String accessKeySecret = "zgNJtwNL2LZzDIS4Way5Rf45pdicTC";

    public static final String bucketName = "helloworldolo";

    private static final String callbackUrl="";

    /**
     * 实现资源的上传
     * @param data 上传的内容
     * @param objname 对象名称
     * @return 访问路径
     * */
    public static String upload(String objname, byte[] data){
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 上传文件到指定的存储空间（bucketName）并将其保存为指定的文件名称（objName）
        ossClient.putObject(bucketName, objname, new ByteArrayInputStream(data));
        /**
         *generatePresignedUrl 生成访问的链接地址
         * 1.存储空间名称
         * 2.对象名称 文件名
         * 3.有效期
         * */

        String url=ossClient.generatePresignedUrl(bucketName, objname, getDate(3)).toString();
        // 关闭OSSClient。
        ossClient.shutdown();
        return url;
    }
    /**
     * 实现资源的上传
     * @param data 上传的内容
     * @param objname 对象名称
     * @param time 链接的有效期
     * @param timeUnit 时间单位
     * @return 访问路径
     * */
    public static String upload(String objname, byte[] data, int time, TimeUnit timeUnit){
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 上传文件到指定的存储空间（bucketName）并将其保存为指定的文件名称（objName）
        ossClient.putObject(bucketName, objname, new ByteArrayInputStream(data));
        /**
         *generatePresignedUrl 生成访问的链接地址
         * 1.存储空间名称
         * 2.对象名称 文件名
         * 3.有效期
         * */
        String url=ossClient.generatePresignedUrl(bucketName,objname,getDate(time, timeUnit)).toString();
        // 关闭OSSClient。
        ossClient.shutdown();
        return url;
    }
    /**
     * 实现字符串的上传
     * @param content 字符串的内容
     * @param objname 对象名称
     * @return 访问路径
     * */
    public static String upload(String objname,String content){
        return upload(objname,content.getBytes());
    }

    private static Date getDate(int years){
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.YEAR,years);
        return calendar.getTime();
    }
    public static Date getDate(int time,TimeUnit timeUnit){
        Calendar calendar=Calendar.getInstance();
        switch (timeUnit){
            case SECONDS:
                calendar.add(Calendar.SECOND,time);break;
            case MINUTES:
                calendar.add(Calendar.MINUTE,time);break;
            case HOURS:calendar.add(Calendar.HOUR,time);break;
            default:calendar.add(Calendar.DAY_OF_MONTH,time);break;
        }
        return calendar.getTime();
    }

    public static String uploadFile(String objname,String filepath) throws Throwable {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        ObjectMetadata meta = new ObjectMetadata();
        //指定上传的内容类型。
        meta.setContentType("text/plain");
        // 通过UploadFileRequest设置多个参数。
        UploadFileRequest uploadFileRequest = new UploadFileRequest(bucketName,objname);
        // 指定上传的本地文件。
        uploadFileRequest.setUploadFile(filepath);
        // 指定上传并发线程数，默认为1。
        uploadFileRequest.setTaskNum(5);
        // 指定上传的分片大小。
        uploadFileRequest.setPartSize(1 * 1024 * 1024);
        // 开启断点续传，默认关闭。
        uploadFileRequest.setEnableCheckpoint(true);
        // 记录本地分片上传结果的文件。
        uploadFileRequest.setCheckpointFile("ossupload.txt");
        // 文件的元数据。
        uploadFileRequest.setObjectMetadata(meta);
        // 设置上传成功回调，参数为Callback类型。
        uploadFileRequest.setCallback(new Callback());

        // 断点续传上传。
        ossClient.uploadFile(uploadFileRequest);
        String url=createURL(objname,ossClient);
        // 关闭OSSClient。
        ossClient.shutdown();
        return url;
    }
    /**
     * 根据对象名称创建访问链接
     * @param objname 对象名称
     * @return 访问地址*/
    private static String createURL(String objname, OSS client){
        return client.generatePresignedUrl(bucketName,objname,getDate(3)).toString();
    }
    /**
     * 对上传的文件名进行重命名并控制长度*/
    public static String rename(String file){
        if(file.length()>30){
            file=file.substring(file.length()-30);
        }
        return UUID.randomUUID().toString().replaceAll("-","")+"_"+file;
    }
}
