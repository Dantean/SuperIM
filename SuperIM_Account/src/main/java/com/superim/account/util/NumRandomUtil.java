package com.superim.account.util;

import java.util.Random;


/**
 * @author HZF
 */
public class NumRandomUtil {
    /**
     * 随机生成 指定长度的数字
     * @param len 长度
     * @return
     */
    public static int createNum(int len) {
        Random random = new Random();

        /**
         * random.nextInt(1)   [0 ~1)
         * Math.pow(10,2)   10^2
         */
        return (int)(Math.pow(10,len-1) + random.nextInt((int)(Math.pow(10,len) - Math.pow(10,len-1))));
    }
}
