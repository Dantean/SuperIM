package com.superim.account.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;

/**
 * @author HZF
 */
public class EncryptUtil {
    public static final String SHA1 = "SHA-1";
    public static final String SHA256 = "SHA-256";
    public static final String SHA384 = "SHA-384";
    public static final String SHA512 = "SHA-512";

    public static final String PUBKEY = "public_key";
    public static final String PRIKEY = "private_key";

    public static String base64enc(String msg) {
        return Base64.getEncoder ().encodeToString (msg.getBytes ());
    }

    private static String base64encByte(byte[] msg) {
        return Base64.getEncoder ().encodeToString (msg);
    }

    private static byte[] base64decByte(String msg) {
        return Base64.getDecoder ().decode (msg);
    }

    public static String base64dec(String msg) {
        return new String (Base64.getDecoder ().decode (msg));
    }

    public static String md5(String msg) {
        try {
            //创建摘要算法对象
            MessageDigest messageDigest = MessageDigest.getInstance ("MD5");
            messageDigest.update (msg.getBytes ());
            return base64encByte (messageDigest.digest ());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace ();
        }
        return null;
    }

    public static String sha(String type, String msg) {
        try {
            //创建摘要算法对象
            MessageDigest messageDigest = MessageDigest.getInstance (type);
            messageDigest.update (msg.getBytes ());
            return base64encByte (messageDigest.digest ());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace ();
        }
        return null;
    }

    public static String createAESKey() {
        try {
            KeyGenerator generator = KeyGenerator.getInstance ("AES");
            generator.init (128);
            SecretKey key = generator.generateKey ();
            return base64encByte (key.getEncoded ());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace ();
        }
        return null;
    }

    public static String aesenc(String key, String msg) {
        SecretKeySpec secretKeySpec = new SecretKeySpec (base64decByte (key), "AES");
        try {
            Cipher cipher = Cipher.getInstance ("AES");
            cipher.init (Cipher.ENCRYPT_MODE, secretKeySpec);
            return base64encByte (cipher.doFinal (msg.getBytes ()));
        } catch (Exception e) {
            e.printStackTrace ();
        }
        return null;
    }

    public static HashMap<String, String> createRSAKey() {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance ("RSA");
            KeyPair keyPair = generator.generateKeyPair ();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate ();
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic ();
            HashMap<String, String> keys = new HashMap<> ();
            keys.put (PUBKEY, base64encByte (publicKey.getEncoded ()));
            keys.put (PRIKEY, base64encByte (privateKey.getEncoded ()));
            return keys;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace ();
        }
        return null;
    }

    public static String rsaEnc(String key, String msg) {
        try {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec (base64decByte (key));
            KeyFactory keyFactory = KeyFactory.getInstance ("RSA");
            PrivateKey privateKey = keyFactory.generatePrivate (keySpec);
            Cipher cipher = Cipher.getInstance ("RSA");
            cipher.init (Cipher.ENCRYPT_MODE, privateKey);
            return base64encByte (cipher.doFinal (msg.getBytes ()));
        } catch (Exception e) {
            e.printStackTrace ();
        }
        return null;
    }

    public static String rsaDec(String key, String msg) {
        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec (base64decByte (key));
            KeyFactory keyFactory = KeyFactory.getInstance ("RSA");
            PublicKey publicKey = keyFactory.generatePublic (keySpec);
            Cipher cipher = Cipher.getInstance ("RSA");
            cipher.init (Cipher.DECRYPT_MODE, publicKey);
            return new String (cipher.doFinal (base64decByte (msg)), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace ();
        }
        return null;
    }
}