package com.superim.account.config;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author HZF
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * 定义分隔符,配置Swagger多包
     */
    private static final String SPLINTER = ";";

    private ApiInfo createAi(){
        return new ApiInfoBuilder().title("接口文档").contact(new Contact("Java","","hiprogramming@foxmail.com")).version("1.0").description("在线接口文档,实现接口测试").build();
    }

    @Bean
    public Docket createD(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(createAi()).select().apis(basePackage("com.superim.account.api" + SPLINTER + "com.superim.account.controller")).paths(PathSelectors.any()).build();
    }

    /**
     * 重写basePackage方法，使能够实现多包访问，复制贴上去
     * @return com.google.common.base.Predicate<springfox.documentation.RequestHandler>
     */
    public static Predicate<RequestHandler> basePackage(final String basePackage) {
        return input -> declaringClass(input).transform(handlerPackage(basePackage)).or(true);
    }

    private static Function<Class<?>, Boolean> handlerPackage(final String basePackage)     {
        return input -> {
            // 循环判断匹配
            for (String strPackage : basePackage.split(SPLINTER)) {
                boolean isMatch = input.getPackage().getName().startsWith(strPackage);
                if (isMatch) {
                    return true;
                }
            }
            return false;
        };
    }

    private static Optional<Class<?>> declaringClass(RequestHandler input) {
        return Optional.fromNullable(input.declaringClass());
    }
}
