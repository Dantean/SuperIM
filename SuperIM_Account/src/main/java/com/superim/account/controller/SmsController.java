package com.superim.account.controller;

import com.superim.account.service.intf.SmsService;
import com.superteam.superim.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author HZF
 */
@Api(tags = "验证码发送")
@RestController
@RequestMapping("/SendSMS")
public class SmsController {
    @Resource
    private SmsService SmsService;

    @ApiOperation(value = "发送验证码", notes = "发送手机验证码")
    @PostMapping("/sendCodeSms.do")
    public R sendCode(String phone) {
        return SmsService.sendCodeSms(phone);
    }
}
