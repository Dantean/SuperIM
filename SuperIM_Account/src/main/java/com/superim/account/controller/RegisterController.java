package com.superim.account.controller;

import com.superim.account.service.intf.RegisterService;
import com.superteam.superim.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author HZF
 */
@Api(tags = "用户注册")
@RestController
@RequestMapping("/register")
public class RegisterController {

    @Resource
    private RegisterService registerService;

    @ApiOperation(value = "注册用户", notes = "注册用户")
    @PostMapping("/register.do")
    public R register(String nickName, String password, String phone, MultipartFile file, String code) {
        return registerService.registerUser(nickName, password, phone, file, code);
    }
}
