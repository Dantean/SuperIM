package com.superim.account.controller;

import com.superim.account.service.intf.LegalizeUserService;
import com.superteam.superim.common.config.SystemCofig;
import com.superteam.superim.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author HZF
 */
@Api(tags = "实名认证")
@RestController
@RequestMapping("/legalize")
public class LegalizeController {

    @Resource
    private LegalizeUserService legalizeUserService;

    @ApiOperation(value = "用户实名认证", notes = "用户上传身份证正面")
    @PostMapping("/legalizeUser.do")
    public R legalizeUser(String idName, String idCardNum, String phone, String code, MultipartFile file, HttpServletRequest request) {
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        return  legalizeUserService.legalizeUser(idName, idCardNum, phone, code, file, token);
    }
}
