package com.superim.account.controller;

import com.superim.account.service.intf.AccountService;
import com.superteam.superim.common.config.SystemCofig;
import com.superteam.superim.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author HZF
 */
@Api(tags = "用户登录")
@RestController
@RequestMapping("/account")
public class AccountController {
    @Resource
    private AccountService accountService;

    @ApiOperation(value = "用户登录", notes = "用户账号密码登录")
    @PostMapping("/login.do")
    public R login(String account, String password) {
        return accountService.login(account, password);
    }

    @ApiOperation(value = "用户登录", notes = "用户手机号验证码登录")
    @PostMapping("/loginByPhone.do")
    public R loginByPhone(String account, String phone, String code) {
        return accountService.loginByPhone(account, phone, code);
    }

    @ApiOperation(value = "找回密码", notes = "用户通过输入账户信息找回密码")
    @PostMapping("/retrievePassword.do")
    public R retrievePassword(String account, String phone, String code, String newPassword, String name, String idCardNum) {
        return accountService.retrievePas(account, phone, code, newPassword, name, idCardNum);
    }

    @ApiOperation(value = "修改个人注册信息", notes = "可以修改注册信息")
    @PostMapping("/modifyUserInfo.do")
    public R modifyUserInfo(HttpServletRequest request, MultipartFile newFile, String newPhone, String code, String newNickName) {
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        return accountService.modifyInfo(token, newFile, newPhone, code, newNickName);
    }

    @ApiOperation(value = "用户退出", notes = "用户点击事件进行退出")
    @PostMapping("/loginOut.do")
    public R loginOut(HttpServletRequest request) {
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        return accountService.loginOutUser(token);
    }
}
