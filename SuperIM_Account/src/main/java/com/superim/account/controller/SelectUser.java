package com.superim.account.controller;

import com.superim.account.service.intf.SelectUserByAccount;
import com.superim.account.vo.UserVo;
import com.superteam.superim.common.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-08 17:43
 **/
@Api(tags = "查询")
@RestController
@RequestMapping("/api")
public class SelectUser {
   @Autowired
   private SelectUserByAccount userByAccount;

    @PostMapping("/selectUserOne.do")
    @ApiOperation(value = "查询单个", notes = "查询单个")
    public UserVo selectUserOne(@RequestBody String account){

       return userByAccount.selectUserOne(account);
    }


    @PostMapping("/selectUsers.do")
    @ApiOperation(value = "查询一堆", notes = "查询一堆")
    public  R selectUsers(@RequestBody List accounts){
        return R.ok(userByAccount.selectUsers(accounts));
    }
}
