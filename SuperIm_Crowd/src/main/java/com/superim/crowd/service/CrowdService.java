package com.superim.crowd.service;

import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.user.User;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-06 20:47
 **/
public interface CrowdService {
    R selectAll(String MyAccount);

    R selectAllMember(String CrowdNumber);

    R selectUserRank(String uaccount,String cnumber);

    R updateCname(String uaccount,String cnumber,String name);

    R updateUserRank(String uaccount,String targetAccount,String cnumber,String rank);

    R createCrowd(String uaccount, String name);

    R addUserToCrowd(String uaccount,String targetAccount,String cnumber,String remark);

    R handleInvitationOfCrowd(String uaccount,String cnumber,String status);

    R removeCrowd(String uaccount, String cnumber);


}
