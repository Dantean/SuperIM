package com.superim.crowd.service;

import com.superim.account.vo.UserVo;
import com.superteam.superim.common.vo.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-08 17:23
 **/
@FeignClient(value = "AccountServer")
public interface UserService {

    @PostMapping("/api/selectUserOne.do")
    UserVo selectUserOne(@RequestBody String account);


    @PostMapping("/api/selectUsers.do")
    List<UserVo> selectUsers(List accounts);

}
