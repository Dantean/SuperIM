package com.superim.crowd.service.impl;

import com.superim.account.vo.UserVo;
import com.superim.crowd.dao.CrowdDao;
import com.superim.crowd.service.CrowdService;
import com.superim.crowd.service.UserService;
import com.superteam.superim.common.dto.CrowdDto;
import com.superteam.superim.common.vo.R;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-06 20:48
 **/
@Service
public class CrowdServiceImpl implements CrowdService {
    @Resource
    private CrowdDao crowdDao;

    @Resource
    private UserService userService;

    /**查询该用户加入的所有群聊
     *
     * @param MyAccount 用户账号
     * @return
     */
    @Override
    public R selectAll(String MyAccount) {
        List<CrowdDto> list = crowdDao.selectAllCrowd(MyAccount);
        return R.ok(list);
    }

    /**查询该群聊中所有用户
     *
     * @param crowdNumber 群号
     * @return
     */
    @Override
    public R selectAllMember(String crowdNumber) {
        return R.ok(crowdDao.selectAllMembers(crowdNumber)) ;
    }

    /**查询该群聊中目标用户权限等级
     *
     * @param uaccount 用户账号
     * @param cnumber  群号
     * @return
     */
    @Override
    public R selectUserRank(String uaccount, String cnumber) {
        return R.ok(crowdDao.selectUserRank(cnumber,uaccount));
    }

    /**修改群名
     *
     * @param uaccount 用户账号
     * @param cnumber 群号
     * @param name 群名字
     * @return
     */
    @Override
    public R updateCname(String uaccount,String cnumber, String name) {
        String rank = crowdDao.selectUserRank(cnumber, uaccount);
        if(rank.equals("管理员")||rank.equals("群主")){
            if (crowdDao.updateCname(cnumber,name) > 0) {
                return R.ok("修改成功");
            }
            return R.fail("哦呵,GG");
        } else {
            return R.fail("凡事问问自己配不配");
        }

    }

    /**修改群成员权限
     *
     * @param uaccount 当前用户账号
     * @param targetAccount 目标用户账号
     * @param cnumber 群号
     * @return
     */
    @Override
    public R updateUserRank(String uaccount, String targetAccount, String cnumber,String rank) {
        String currentUserRank = crowdDao.selectUserRank(cnumber, uaccount);
        System.out.println("当前用户登记" +currentUserRank);
        if(currentUserRank.equals("群主")||currentUserRank.equals("管理员")){
            String targetUserRank = crowdDao.selectUserRank(cnumber, targetAccount);
            System.out.println("目标登记" + targetUserRank);
            if (!(targetUserRank.equals("群主"))) {
                Map<String,Object> map = new  HashMap<>();
                map.put("rank",rank);
                map.put("targetAccount",targetAccount);
                map.put("cnumber",cnumber);
                if (crowdDao.updateUserRank(map) > 0) {
                    return R.ok("权限设置成功");
                }
                return R.fail("设置失败");
            }
              else {
                return R.fail("尔等胆敢对龙王动手动脚 (╯‵□′)╯︵┻━┻");
            }
        }
        else {
           return R.fail("凡事问问自己配不配");
        }

    }

    /**创建群聊
     *
     * @param uaccount 用户账号
     * @param name 群名字
     * @return
     */
    @Override
    @Transactional
    public R createCrowd(String uaccount, String name) {
        String last = crowdDao.selectLastCAccount();
        String newNumber = (Integer.parseInt(last) + 1) + "";
        System.out.println(" 新号为" + newNumber);
        Map<String,Object> map = new  HashMap<>();
        map.put("name",name);
        map.put("cnumber",newNumber);
        map.put("population",1);
        //创建群聊
        crowdDao.createCrowd(map);

        map.put("uaccount",uaccount);
        map.put("rank","群主");
        //创建映射表
        if (crowdDao.createJoint(map) > 0) {
            return R.ok(map);
        }
        return  R.fail("创建失败");

    }

    /**邀请用户入群
     *
     * @param uaccount  当前账号
     * @param targetAccount 目标账号
     * @param cnumber 群号
     * @param remark 备注信息
     * @return
     */
    @Override
    public R addUserToCrowd(String uaccount, String targetAccount, String cnumber, String remark) {
            String temp = targetAccount==null?uaccount:targetAccount;
            List<CrowdDto> list = crowdDao.selectAllCrowd(temp);
            for (CrowdDto crowdDto : list) {
                if (crowdDto.getCnumber().equals(cnumber)) {
//                如果这个人这群里,则返回他的信息
                    UserVo userVo = userService.selectUserOne(temp);
                    System.out.println(userVo);
                    return R.ok(userVo);
                }
            }

            Map<String,Object> map = new  HashMap<>();
            map.put("uaccount",uaccount);
            map.put("cnumber",cnumber);
            map.put("targetAccount", targetAccount);
            map.put("remark",remark);
            return R.ok(crowdDao.addUserToCrowd(map));


    }

    /**处理群邀请信息
     *
     * @param uaccount 当前账号
     * @param cnumber 群号
     * @param status 状态
     * @return
     */
    @Override
    @Transactional
    public R handleInvitationOfCrowd(String uaccount, String cnumber, String status) {
        List<CrowdDto> list = crowdDao.selectAllCrowd(uaccount);
        for (CrowdDto crowdDto : list) {
            if (crowdDto.getCnumber().equals(cnumber)) {
                return R.fail("已经是该群成员了");

            }
        }

        if (status.equals("通过")) {
            Map<String,Object> map = new  HashMap<>();
            map.put("uaccount",uaccount);
            map.put("cnumber",cnumber);
            map.put("rank","成员");
            crowdDao.enterToCrowd(map);
            map.put("status",status);
            map.put("targetAccount",uaccount);
            crowdDao.updateApplication(map);
            crowdDao.updatePopulation(cnumber);
        }


        return R.ok("加入群聊成功");
    }

    /**解散群聊
     *
     * @param uaccount 用户账号
     * @param cnumber 群号
     * @return
     */
    @Override
    @Transactional
    public R removeCrowd(String uaccount, String cnumber) {
        String rank = crowdDao.selectUserRank(cnumber, uaccount);
        if (rank.equals("群主")) {
//            删除群聊
            crowdDao.deleteCrowd(cnumber);
            Map<String,Object> map = new  HashMap<>();
            map.put("cnumber",cnumber);
            map.put("uaccount",uaccount);
//            删除映射
            if (crowdDao.deleteJoint(map)> 0) {
                return R.ok("好聚好散吧");
            }
            return R.fail("删除失败");
        }
        return R.fail("凡事问问自己配不配");
    }
}
