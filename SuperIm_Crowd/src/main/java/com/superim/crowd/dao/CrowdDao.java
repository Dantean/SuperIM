package com.superim.crowd.dao;

import com.superteam.superim.common.dto.CrowdDto;
import com.superteam.superim.common.dto.CrowdMemberDto;
import com.superteam.superim.entity.user.User;


import java.util.List;
import java.util.Map;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-06 20:47
 **/
public interface CrowdDao {
    //查询当前用户加入的所有的群
    List<CrowdDto> selectAllCrowd(String MyAccount);

    List<CrowdMemberDto> selectAllMembers(String crowdNumber);

    String selectUserRank(String cnumber,String uaccount);

    String selectLastCAccount();

    int updateCname(String cnumber,String name);

    int updateUserRank(Map<String,Object> map);

    int createCrowd(Map<String,Object> map);

    int createJoint(Map<String,Object> map);

    int addUserToCrowd(Map<String,Object> map);

    int deleteCrowd(String cnumber);

    int deleteJoint(Map<String,Object> map);

    int enterToCrowd(Map<String,Object> map);

    int updatePopulation(String cnumber);

    int updateApplication(Map<String,Object> map);
}
