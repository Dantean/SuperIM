package com.superim.crowd.controller;

import com.superim.crowd.service.CrowdService;
import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.config.SystemCofig;
import com.superteam.superim.common.util.RedissonUtil;
import com.superteam.superim.common.vo.R;
import com.superteam.superim.entity.user.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-06 20:46
 **/
@Api(tags = "群聊管理")
@RestController
@RequestMapping("/crowd")
public class CrowdController {
    @Autowired
    private CrowdService service;

    /**
     * 查用户加入的所有群
     * @param request 请求
     * @return 用户加入的所有群
     */
    @GetMapping("/selectAllCrowd.do")
    @ApiOperation(value = "查该用户加入的所有群", notes = "查该用户加入的所有群")
    public R selectAllCrowd(HttpServletRequest request){
        //获取当前登录用户的token
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        //根据当前用户获取群组数据
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return service.selectAll(user.getAccount());
    }

     /**
     * 查目标群中的所有用户
     * @param cnumber 群号
     * @return 目标群中的所有用户
     */
    @GetMapping("/selectAllUser.do")
    @ApiOperation(value = "查目标群中的所有用户", notes = "查目标群中的所有用户")
    public R selectAllUser(String cnumber){
        return service.selectAllMember(cnumber);
    }


    /**
     * 查目标群中的目标用户的权限等级
     * @param cnumber 群号
     * @return 目标群中的所有用户
     */
    @GetMapping("/selectUserRank.do")
    @ApiOperation(value = "查目标群中的目标用户的权限等级", notes = "查目标群中的目标用户的权限等级")
    public R selectUserRank(HttpServletRequest request,String cnumber){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return service.selectUserRank(user.getAccount(),cnumber);
    }


    /**
     * 修改目标群名
     * @param request 请求
     * @param name 群名称
     * @return
     */
    @PutMapping("/updateCname.do")
    @ApiOperation(value = "修改群名", notes = "修改群名")
    public R updateCname(HttpServletRequest request,String cnumber,String name){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return service.updateCname(user.getAccount(),cnumber,name);
    }

    /**修改目标群中目标用户权限
     *
     * @param request
     * @param cnumber
     * @param targetAccount
     * @param rank
     * @return
     */
    @PutMapping("/updateUserRank.do")
    @ApiOperation(value = "修改目标群中目标用户权限", notes = "修改目标群中目标用户权限")
    public R updateUserRank(HttpServletRequest request,String cnumber,String targetAccount,String rank){
        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);

        return service.updateUserRank(user.getAccount(),targetAccount, cnumber, rank);

    }

    /**新建群聊
     *
     * @param request 请求
     * @param name  群名
     * @return
     */
    @PostMapping("/addCrowd.do")
    @ApiOperation(value = "新建群聊", notes = "新建群聊")
    public R addCrowd(HttpServletRequest request,String name){

        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        String remoteAddr = request.getRemoteAddr();
        System.out.println("远程IP===============>" +remoteAddr);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return service.createCrowd(user.getAccount(),name);
    }


    @PostMapping("/addUserToCrowd.do")
    @ApiOperation(value = "邀请/申请 加入群聊", notes = "邀请/申请 加入群聊")
    public R addUserToCrowd(String uaccount,String targetAccount,String cnumber,String remark){

//        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
//        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
//
//       return service.addUserToCrowd(user.getAccount(), targetAccount, cnumber, remark) ;
        return  service.addUserToCrowd(uaccount, targetAccount, cnumber, remark);
    }

    @PostMapping("/handleInvitationOfCrowd.do")
    @ApiOperation(value = "群邀请信息处理", notes = "群邀请信息处理")
    public R handleInvitationOfCrowd(String uaccount,String cnumber,String status){

//        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
//        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
//       return service.addUserToCrowd(user.getAccount(), targetAccount, cnumber, remark) ;

        return service.handleInvitationOfCrowd(uaccount, cnumber, status);
    }



    @DeleteMapping("/deleteCrowd.do")
    @ApiOperation(value = "解散群聊", notes = "解散群聊")
    public R deleteCrowd(HttpServletRequest request,String cnumber){

        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
        return service.removeCrowd(user.getAccount(),cnumber);

    }

//    @DeleteMapping("/deleteUserFromCrowd.do")
//    @ApiOperation(value = "移除目标群中的目标用户", notes = "移除目标群中的目标用户")
//    public R deleteUserFromCrowd(String uaccount,String cnumber,String targetAccount){
//
////        String token = request.getHeader(SystemCofig.HEAD_TOKEN);
////        String remoteAddr = request.getRemoteAddr();
////        System.out.println("远程IP===============>" +remoteAddr);
////        User user = (User)RedissonUtil.getStr(RedisKeyConfig.LOGIN_TOKEN + token);
////        return service.createCrowd(user.getAccount(),name);
//
//
//    }



}
