package com.superim.crowd;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-06 20:43
 **/
@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "com.superim.crowd.dao")
//服务调用
@EnableFeignClients
//服务注册和发现
@EnableDiscoveryClient
public class CrowdApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrowdApplication.class,args);
    }
}
