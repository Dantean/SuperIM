package com.superteam.superim.filter;

import com.superteam.superim.common.config.RedisKeyConfig;
import com.superteam.superim.common.config.SystemCofig;
import com.superteam.superim.common.util.RedissonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @program: superim
 * @description: 检验用户是否登录
 * @author: ErBa(韩智飞)
 * @create: 2020-10-05 19:00
 **/
@Slf4j
@Component
public class MyFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        // 拦截.do接口
        if (request.getPath().value().endsWith(".do")) {
            HttpHeaders headers = request.getHeaders();
            String token = headers.getFirst(SystemCofig.HEAD_TOKEN);
            // 验证redis是否有这个token
            if (RedissonUtil.checkKey(RedisKeyConfig.LOGIN_TOKEN + token)) {
                // 放行
                return chain.filter(exchange);
            } else {
                ServerHttpResponse response = exchange.getResponse();
                response.setStatusCode(HttpStatus.BAD_REQUEST);
                //响应结果数据
                DataBuffer buffer = response.bufferFactory().wrap("请先登录".getBytes());
                //拦截本次请求并返回数据
                return response.writeWith(Mono.just(buffer));
            }
        } else {
            return chain.filter(exchange);
        }

    }
}
