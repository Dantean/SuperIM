package com.superteam.superim.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @program: LiveRoom
 * @description:
 * @author: Feri(邢朋辉)
 * @create: 2020-09-27 10:20
 */
public class DateUtil {
    //比较 日期是否相同
    public static boolean checkDate(Date ctime, Date stime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(ctime).equals(sdf.format(stime));
    }

    //验证是否为昨天
    public static boolean checkYesdate(Date ctime) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return checkDate(ctime, calendar.getTime());
    }

    //验证断签是否超过365天
    public static boolean checkYear(Date ctime) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -365);
        return ctime.getTime() < calendar.getTime().getTime();
    }
}
