package com.superteam.superim.common.dto;

import lombok.Data;

/**
 * @program: LiveRoom
 * @description:
 * @author: Feri(邢朋辉)
 * @create: 2020-09-29 10:01
 */
@Data
public class GiftDto {
    private Integer id;
    private Integer count;
    private Integer roomid;
    private Integer price;
    private Integer type;//形式  1.兑换 2.购买
}
