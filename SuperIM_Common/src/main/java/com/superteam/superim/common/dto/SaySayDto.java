package com.superteam.superim.common.dto;

import com.superteam.superim.entity.SaySayImg;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SaySayDto {
    private Integer id;     //说说id
    private Integer uid;    //发说说的人的id
    private String text;    //说说文本内容
    private Date cTime;     //发说说的时间
    private Integer sid;    //转发人的id

    private List<SaySayImg> saySayImgList;      //图片
}
