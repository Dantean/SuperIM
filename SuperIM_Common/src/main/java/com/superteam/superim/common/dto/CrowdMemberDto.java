package com.superteam.superim.common.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import sun.dc.pr.PRError;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-07 17:38
 **/
@Data
@ToString
@NoArgsConstructor
public class CrowdMemberDto {
    private String uaccount;
    private String urank;
}
