package com.superteam.superim.common.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-07 16:05
 **/
@Data
@ToString
@NoArgsConstructor
public class CrowdDto {
    private Integer id;
    private String name;
    private String cnumber;
    private Integer population;
    private String urank;
    private String crank;
}
