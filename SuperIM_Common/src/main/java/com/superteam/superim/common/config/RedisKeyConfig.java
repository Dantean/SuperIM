package com.superteam.superim.common.config;

/**
 * @program: LiveRoom
 * @description:
 * @author: Feri(邢朋辉)
 * @create: 2020-09-28 09:57
 */
public class RedisKeyConfig {
    /**String类型 记录登录的账号,因为账户是唯一的  值：存储令牌*/
    public static final String LOGIN_ACCOUNT = "IM:login:account:";
    /**String类型 记录登录的令牌 对应的用户信息  值：用户信息 序列化存储*/
    public static final String LOGIN_TOKEN = "IM:login:token";
    /**String类型 唯一登录 记录被挤掉的令牌*/
    public static final String LOGIN_JD = "IM:login:jd";
    //list类型 记录好友列表 当前登录账号的好友列表
    public static final String LOGIN_ACCOUNT_Friends =  "IM:login:account:Friends:";
    /**String类型 记录发送的验证码*/
    public static final String SMS_CODE = "IM:sms:code:";
    //String类型 说说的id  记录点赞总数
    public static final String PRAISE_COUNT = "IM:saySay:praise:count:";
    //list类型  说说的id  记录所有给本条说说点赞的人
    public static final String PRAISE_USERS = "IM:saySay:praise:users:";
    //String类型  说说的id  记录本条说说的浏览量
    public static final String SAYSAY_VIEWS = "IM:saySay:views:";
    /**记录私聊聊天信息的redis 缓存*/
    public static final String SINGLE_MESSAGE = "IM:chat:single";
    /**记录群聊聊天信息的redis 缓存*/
    public static final String GROUP_MESSAGE = "IM:chat:group";
}
