package com.superteam.superim.common.dto;

import lombok.Data;

import java.util.Date;

/**
 * @program: LiveRoom
 * @description:
 * @author: Feri(邢朋辉)
 * @create: 2020-09-28 09:39
 */
@Data
public class ChatMsg {
    private int type;//消息类型：1.系统消息 2.主播消息 3.Vip会员消息 4.会员消息
    private String msg;//消息内容
    private String user;//会员id 如果是系统消息 显示-1
    private Date ctime;//时间

    public static ChatMsg setMsg(int type, String msg, String user) {
        ChatMsg chatMsg = new ChatMsg();
        chatMsg.setMsg(msg);
        chatMsg.setType(type);
        chatMsg.setUser(user);
        chatMsg.setCtime(new Date());
        return chatMsg;
    }
}