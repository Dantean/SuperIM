package com.superteam.superim.common.util;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @program: BootTem2003
 * @description: 基于Redisson操作Redis数据库
 * @author: Feri(邢朋辉)
 * @create: 2020-09-11 14:56
 */
public class RedissonUtil {
    private static String host="47.98.191.158";
    private static int port=6379;
    private static String pass="qwer";
    private static RedissonClient client;
    static {
        Config config=new Config();
        config.useSingleServer().setPassword(pass).setAddress("redis://"+host+":"+port);
        client=Redisson.create(config);
    }

    //新增
    public static void addStr(String key,Object val){
        client.getBucket(key).set(val);
    }
    public static void addStrTime(String key,Object val,int seconds){
        client.getBucket(key).set(val,seconds, TimeUnit.SECONDS);
    }
    public static void addHash(String key,String field,Object data){
        client.getMap(key).put(field,data);
    }
    public static void addList(String key, Object data) {
        client.getList(key).add(data);
    }
    public static void addList(String key, Collection<Object> data) {
        client.getList(key).addAll(data);
    }



    //查询
    public static Object getStr(String key){
        return client.getBucket(key).get();
    }
    public static Object getHash(String key,String field){
        return client.getMap(key).get(field);
    }
    public static Collection<Object> getHashVals(String key){
        return client.getMap(key).readAllValues();
    }
    public static Map<Object,Object> getHashAll(String key){
        return client.getMap(key).readAllMap();
    }
    public static List<Object> getListAll(String key) {
        return client.getList(key).readAll();
    }

    //校验
    public static boolean checkKey(String key){
        return client.getKeys().countExists(key)>0;
    }
    public static boolean checkHashField(String key,String f){
        return client.getMap(key).containsKey(f);
    }
    //删除Key
    public static boolean delKey(String... key){
        return client.getKeys().delete(key)>0;
    }
    public static boolean delField(String key,String field){
        return client.getMap(key).remove(field)!=null;
    }
    public static boolean delList(String key) {
        return client.getList(key).delete();
    }
    public static boolean delListField(String key, Object data) {
        return client.getList(key).remove(data);
    }
}
