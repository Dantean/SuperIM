package com.superteam.superim.common.util;

import com.baidu.aip.contentcensor.AipContentCensor;
import org.json.JSONObject;

/**
 * @program: CodingsSart
 * @description: 基于百度云的内容审核实现文本、图片等审核：防止出现：色情、暴力、涉恐等
 * @author: Feri(邢朋辉)
 * @create: 2020-09-16 09:42
 */
public class ContentCensorUtil {
    //设置APPID/AK/SK
    public static final String APP_ID = "17214730";
    public static final String API_KEY = "WnrcZnQNbveI7UG7sDroOm1K";
    public static final String SECRET_KEY = "FUui3jN440l9eLN8B335C3VS62HvtafU";
    private static AipContentCensor client;

    static {
        // 初始化一个AipContentCensor
        client = new AipContentCensor(APP_ID, API_KEY, SECRET_KEY);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
    }

    //审核文本内容
    public static boolean censorText(String msg) {
        JSONObject object = client.textCensorUserDefined(msg);
        if (object != null) {
            System.out.println(object);
            if (object.getInt("conclusionType") == 1) {
                return true;
            }
        }
        return false;
    }

    //审核图片
    public static boolean censorImg(byte[] data) {
        JSONObject object = client.imageCensorUserDefined(data, null);
        if (object != null) {
            System.out.println(object);
            if (object.getInt("conclusionType") == 1) {
                return true;
            }
        }
        return false;
    }


}
