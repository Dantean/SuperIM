package com.superteam.superim.entity.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author HZF
 */
@Data
@NoArgsConstructor
public class LoginOut {
    private Integer id;
    private Date loginTime;
    private Date quitTime;
    private String loginIp;
    private String quitIp;
    private String loginAddress;
    private String quitAddress;
    private Integer uid;
}
