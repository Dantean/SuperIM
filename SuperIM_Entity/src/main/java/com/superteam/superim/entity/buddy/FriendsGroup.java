package com.superteam.superim.entity.buddy;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/6
 * Time: 10:46
 * Code introduction:
 */

@Data
public class FriendsGroup {
    private String account;
    private String grouping;
}
