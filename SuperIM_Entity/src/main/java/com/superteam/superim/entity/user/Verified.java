package com.superteam.superim.entity.user;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实名认证
 * @author HZF
 */
@Data
@NoArgsConstructor
public class Verified {
    private Integer id;
    private String idName;
    private String idCardNum;
    private String address;
    private String idCardPositive;
    private String nationality;
    private String sex;
    private String birth;
    private Integer uid;
}
