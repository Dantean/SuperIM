package com.superteam.superim.entity.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author HZF
 */
@Data
@NoArgsConstructor
public class User implements Serializable {
    private Integer id;
    private String nickName;
    private String account;
    private String password;
    private String phone;
    private String photo;
}
