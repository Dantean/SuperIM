package com.superteam.superim.entity.buddy;

import com.sun.xml.internal.bind.v2.model.core.ID;
import lombok.Data;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/5
 * Time: 11:32
 * Code introduction:
 */

@Data
public class FriendsList {
    private String MyAccount;//我的账号
    private String FriendsAccount;//朋友账号
    private Date c_time;//好友添加时间
    private String remarks;//备注
    private String grouping;//好友所属分组
}
