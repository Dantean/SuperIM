package com.superteam.superim.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SaySay implements Serializable {
    private Integer id;     //说说id
    private Integer uid;    //发说说的人的id
    private String text;    //说说文本内容
    private Date cTime;     //发说说的时间
    private Integer sid;    //转发说说的id
}
