package com.superteam.superim.entity.buddy;

import lombok.Data;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/10/6
 * Time: 14:07
 * Code introduction:
 */

@Data
public class ApplicationAddFriendList {
    private String applicant;//申请人
    private String reviewer;//审核人
    private String status;//状态
    private String remarks;//备注信息
    private Date ctime;//申请时间
}
