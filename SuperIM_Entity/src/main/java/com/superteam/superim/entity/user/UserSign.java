package com.superteam.superim.entity.user;

import lombok.Data;

import java.util.Date;

/**
 * @program: superim
 * @description:
 * @author: ErBa(韩智飞)
 * @create: 2020-10-05 15:11
 **/
@Data
public class UserSign {
    private Integer id;
    private Integer uid;    // 用户id
    private Date stime;     // 签到时间
    private Integer days;   // 连续签到天数

}
