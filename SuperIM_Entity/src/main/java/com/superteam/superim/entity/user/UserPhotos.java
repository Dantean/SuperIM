package com.superteam.superim.entity.user;

import lombok.Data;

import java.util.Date;

/**
 * @program: superim
 * @description: 我的相册
 * @author: ErBa(韩智飞)
 * @create: 2020-10-07 13:00
 **/
@Data
public class UserPhotos {
    Integer id;
    Integer uid;
    String photo;
    Double longitude;   // 经度
    Double latitude;    // 纬度
    Date ptime;         // 拍照时间
    Date utime;         // 上传时间
    String country;     // 国家
    String province;    // 身份
    String city;        // 地级市
    String district;    // 区域


}
