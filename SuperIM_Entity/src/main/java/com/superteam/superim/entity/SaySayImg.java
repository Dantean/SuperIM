package com.superteam.superim.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SaySayImg {
    private Integer id;         //图片id
    private String imgPath;     //图片路径
    private Integer sid;        //说说id
}
