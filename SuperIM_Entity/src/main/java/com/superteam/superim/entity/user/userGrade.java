package com.superteam.superim.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李德硕
 * @version 1.0.0
 * @ClassName userGrade.java
 * @Description TODO
 * @createTime 2020年10月08日 16:10:00
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class userGrade {

    //用户表和等级表的关联表 id
    private Integer id;
    //用户id
    private Integer uid;
    //等级id
    private Integer gid;

}
