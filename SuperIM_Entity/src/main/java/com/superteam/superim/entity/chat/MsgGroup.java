package com.superteam.superim.entity.chat;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* @description: 
* @author: hiseage
* @create: 2020-10-06 14:04:45
*/
@Data
public class MsgGroup implements Serializable {
    private Integer msgId;
    private Integer fromId;
    private Integer groupId;
    private String content;
    private Date createTime;

    public MsgGroup() {
    }

    public MsgGroup(Integer fromId, Integer groupId, String content, Date createTime) {
        this.fromId = fromId;
        this.groupId = groupId;
        this.content = content;
        this.createTime = createTime;
    }
}

