package com.superteam.superim.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Reply {
    private Integer id;     //回复评论的id
    private Integer sid;    //说说的id
    private Integer uid;    //用户的id
    private Integer ruid;   //被回复用户的id
    private String comment;     //回复的内容
    private Date commentTime;       //回复的时间
}
