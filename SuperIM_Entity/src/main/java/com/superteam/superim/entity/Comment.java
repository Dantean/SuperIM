package com.superteam.superim.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Comment {
    private Integer id;     //评论id
    private Integer sid;    //说说id
    private Integer uid;    //用户id
    private String comment;     //评论内容
    private Date commentTime;   //评论时间
}
