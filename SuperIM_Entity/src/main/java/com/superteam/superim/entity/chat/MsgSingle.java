package com.superteam.superim.entity.chat;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* @description: 
* @author: hiseage
* @create: 2020-10-06 14:04:45
*/
@Data
public class MsgSingle implements Serializable {
    private Integer msgId;
    private Integer fromId;
    private Integer toId;
    private String content;
    private Date createTime;

    public MsgSingle() {
    }

    public MsgSingle(Integer fromId, Integer toId, String content, Date createTime) {
        this.fromId = fromId;
        this.toId = toId;
        this.content = content;
        this.createTime = createTime;
    }
}

