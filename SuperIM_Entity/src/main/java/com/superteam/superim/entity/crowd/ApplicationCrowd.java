package com.superteam.superim.entity.crowd;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: superim
 * @description:
 * @author: Camus(靳翔)
 * @create: 2020-10-06 21:43
 **/
@Data
@ToString
@NoArgsConstructor
public class ApplicationCrowd implements Serializable {
    private Integer id;
    private String application;
    private Integer crowdId;
    private String status;
    private Date ctime;
    private String remark;
}
