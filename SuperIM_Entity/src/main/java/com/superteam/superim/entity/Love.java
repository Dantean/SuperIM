package com.superteam.superim.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Love {
    private Integer id;         //收藏id
    private Integer uid;        //用户id
    private Integer sid;        //说说id
    private Date lTime;         //收藏时间
}
