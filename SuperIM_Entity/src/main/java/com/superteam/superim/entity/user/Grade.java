package com.superteam.superim.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李德硕
 * @version 1.0.0
 * @ClassName Grade.java
 * @Description TODO
 * @createTime 2020年10月08日 16:10:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Grade {

    //等级表 id
    private Integer id;
    //等级表 等级
    private String grade;
}
